<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Properties extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
    {
        parent::__construct();
        $this->load->model('Property');
        $this->load->model('utility');
        $this->load->model('Generic');
        $this->load->helper('text');
    }
    
	public function details($slug=null)
    {
        if(!isset($slug)|| empty($slug))
        {
            redirect('');
        }

        //$biz_data  = $this->Generic->getByFieldSingle('slug',$slug, $tablename="tbl_property");// get from db
        $data_check = array(  
        					'slug'  => $slug,
                            'status'  => 1
                            );
         $biz_data =  $this->Generic->findByCondition($data_check,'tbl_property'); 
         //print("<pre>".print_r($biz_data,true)."</pre>");die;
        if($biz_data)
        {
        	$prop = $this->Property->getAll(array('propertyslug'=>$slug));
        	$data_check = array(  
                                 'propertyid'  => $biz_data[0]['id'],
                                 'status' =>  1
                                 );
        	$propimg =  $this->Generic->findByCondition($data_check,'tbl_gallary');
        	$data['prop'] = $prop;
        	$data['propimg'] = $propimg;//print("<pre>".print_r( $propimg,true)."</pre>");die;
			$data['icon'] = 'icon.ico';
			$data['nav'] = '';

	        $data ['meta_keyword']= 'DevHomes,Nigerial,Estate Management Service';
	        $data['titel'] = 'DevHomes Estate Management :- Property Category';
	        $data['error_page'] = 'included/error_page';

	        $data ['page_loader']= 'mainpage/details';

			$this->load->view('mainpage/layout',$data);
		}
        else
        {
            redirect('');
        }
	}
}
