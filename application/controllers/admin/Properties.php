<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Properties extends CI_Controller {
    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Property');
        $this->load->model('utility');
        $this->load->model('Generic');
        $this->load->helper('text');
    }
    
    public function index()
    {
        //print_r($_SESSION);
        $this->validateadmin();
            
       
        $data['error_page'] = 'admin/access';
        $data ['meta_keyword']= 'DevHomes Property Management Service,Dashboard';
        $data['titel'] = 'DevHomes Property Management :- Property Category';
        $data['pageheader'] = "Property Gallary Projects List";
        $data['breadCrumbs'] = '<li class="breadcrumb-item active">Property Table</li>';
        $data['mainmenu'] = "properties";
        
        // Load all the impacts
        //$data['prop'] = $this->Property->getAll(array('whouploadid'=>$_SESSION['userId']));

        if($_SESSION['role']==ROLE_AGENT)
        {
             $data['prop'] = $this->Property->getAll(array('whouploadid'=>$_SESSION['userId']));
        }
        if($_SESSION['role']==ROLE_LANDLORD)
        {
             $data['prop'] = $this->Property->getAll(array('landlordpropdid'=>$_SESSION['userId']));
        }
        if($_SESSION['role']==ROLE_SUPERADMIN)
        {
             $data['prop'] = $this->Property->getAll();
        }
        
        //print("<pre>".print_r($this->Property->getAll(array('whouploadid'=>$_SESSION['userId'])),true)."</pre>");die;

        $data ['content_file']= 'prop-list';
        $this->load->view('admin/layout', $data);
        
    }
    
    // Validate a admin user login
    function validateadmin()
    {
        if (! isset ( $_SESSION['isLogIn'] ) || $_SESSION['isLogIn'] != TRUE) 
        {
           redirect('admin');
        }
    }

    public function accessdenied()
    {
        $data['icon'] = 'icon.ico';
        $data ['meta_keyword']= 'DevHomes,Nigerial,Property Management Service,Dashboard';
        $data['titel'] = 'DevHomes Property Management :- Property Category';
        $data['pageheader'] = 'Access Denied';

        $data ['content_file']= 'access';
        $this->load->view('admin/layout', $data);
    }

    public function gallery($slug=null)
    {
        //print_r($_SESSION);
        $this->validateadmin();
        if(!isset($slug)|| empty($slug))
        {
            redirect('admin/impact');
        }
        $impact_data  = $this->Generic->getByFieldSingle('slug',$slug, $tablename="tbl_property");// get from db
        if($impact_data)
        {
            $data['icon'] = 'icon.ico';
            $data['error_page'] = 'admin/access';
            $data ['meta_keyword']= 'DevHomes,Nigerial,Property Management Service,Dashboard';
            $data['titel'] = 'DevHomes Property Management :- Property Category';
            $data['pageheader'] = "Property Gallary:-".$impact_data['name'];
            $data['breadCrumbs'] = '<li class="breadcrumb-item "><a href="'.site_url('admin/properties').'">Property Table</a></li> <li class="breadcrumb-item active">'.$impact_data['name'].'</li>';
            $data['mainmenu'] = "properties";
            
            // Load all the gallary
            $data_check = array(  
                                 'id'  =>  $impact_data['id'],
                                 'status' =>  1
                                 );
            $get_data =  $this->Generic->findByCondition($data_check,'tbl_gallary');

            $data['impacts'] = $impact_data;
            $data['impacts_img'] = $this->Property->getPhotoById($impact_data['id']);
            //print("<pre>".print_r($this->Impacts->getPhotoById($impact_data['id']),true)."</pre>");die;
            $data ['content_file']= 'prop-gallary';
            $this->load->view('admin/layout', $data);
        }
        else
        {
            redirect('admin/impact');
        }
    }

    // Controller function to display the add user form
    public function addform($slug=null)
    {
        $this->validateadmin();
        $data['error_page'] = 'admin/access';
        $data ['meta_keyword']= 'DevHomes,Nigerial,Property Management Service,Dashboard';
        $data['titel'] = 'DevHomes Property Management :- Property Category';
        $data['pageheader'] = "New Property Form";
        $data['breadCrumbs'] = '<li class="breadcrumb-item"><a href="'.site_url("admin/properties").'">Property Table</a></li> <li class="breadcrumb-item active">New Property</li>';
        $data['mainmenu'] = "Properpropertiesty";
        $data['state'] =$this->Generic->getAll($tablename='states', $limit=NULL, $fieldlist=null, $createdat=null, $updatedat=null, $orderbyfield='statename');
        $data_check = array(  
                                 'userroleid'  => ROLE_LANDLORD,
                                 'status' =>  1
                                 );
        $landlord =  $this->Generic->findByCondition($data_check,'tbl_adminusers');
        $data['landlord'] = $landlord;

        $data ['content_file']= 'prop_new';
        $this->load->view('admin/layout', $data);
    }

    public function delete_multipleimage() 
    {
        //rmdir($dir); //to delete directory use
        
        $fullpath = './assets/uploads/';
        $thumbpath = './assets/uploads/thumbs/';
        $picture=$_POST["key"];
        
        unlink($fullpath.$picture);
        unlink($thumbpath.$picture);
        $imginfo = $this->Generic->getByFieldSingle('name', $picture, $tablename='tbl_gallary');
        if($imginfo)
        {
            $this->db->delete('tbl_gallary', array('id'=>$imginfo['id']));
            //$this->Generic->delete($imginfo['id'], $tablename="tbl_impactgallary");
        }
        echo true;
        
    }
    public function save_multipleimage($id=null) 
    {
           
           
        $this->load->library('image_lib');
        $this->load->library('upload');
        
        if (!empty($_FILES['files']['name']) )
        {
            $output = '';
            
            
            //$config['image_library'] = 'gd2';
            //$this->load->library('image_lib');
            
            $config['upload_path'] = './assets/uploads/';
            $config['allowed_types'] = 'jpg|jpeg|png';  
            $config['remove_spaces']  = TRUE;
            $config['overwrite'] = TRUE;
            //$this->load->library('upload', $config); 
            
            foreach($_FILES['files']['name'] as $i => $images)
            {
                $exploded = explode('.',$images);
                $file_extn = strtolower(end($exploded));
                //print("<pre>".print_r($i."--".$images,true)."</pre>");
                
                //$ext = end((explode(".", $_FILES['files']['name'][$i])));
                //$file_name = time().$i."_".($i+1).".".$ext;
                
                $newname = $this->utility->generate_random_string(15).".".$file_extn;
                
                $_FILES ['userfile'] ['name'] =         $newname;
                //$_FILES ['userfile'] ['name'] =        $_FILES['files']['name'][$i]; //$newname
                $_FILES ['userfile'] ['type'] =        $_FILES ['files'] ['type'] [$i];
                $_FILES ['userfile'] ['tmp_name'] =    $_FILES ['files'] ['tmp_name'] [$i];
                $_FILES ['userfile'] ['error'] =       $_FILES ['files'] ['error'] [$i];
                $_FILES ['userfile'] ['size'] =        $_FILES ['files'] ['size'] [$i];
                
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('userfile'))
                    {
                        echo $this->upload->display_errors();  
                    }
                else 
                    {
                        $data = $this->upload->data(); 
                        create_thumb($data, '300', '200', './assets/uploads/thumbs/', TRUE);
                        
                        $data_Newimg = array(  
                                'propertyid'  => $id,
                                'name' => $data['file_name']
                                );
                        $insert_data = $this->Generic->add($data_Newimg, $tablename="tbl_gallary");
                        $num = $this->utility->generate_random_string(4);
                        $output .= '
                            <div class="preview-image preview-show-'. $num. '" >
                                <div class="image-cancel" data-no="'. $num .'" data-key="'.$data["file_name"].'" >x</div> 
                                <div class="image-zone">
                                    <img id="pro-img-'.$num . '" src="'.site_url('assets/uploads/thumbs/').$data["file_name"].'"  />
                                </div>
                                <input type="hidden" id="" name="prd_images[]" class="form-control img_prd" value="'.$data["file_name"].'">
                            
                            </div>
                            ';
                    //} 
                    }
                //else{ print_r($this->upload->display_errors()); }
            }
            //print("<pre>".print_r($output,true)."</pre>");die;
            echo $output;
            
        }
            
             
    }
    
    // Controller function to display the edit user form
    public function editform($slug=null)
    {
        $this->validateadmin();
        if(!isset($slug)|| empty($slug))
        {
            redirect('admin/properties');
        }
        $impact_data  = $this->Generic->getByFieldSingle('slug',$slug, $tablename="tbl_property");// get from db
        if($impact_data)
        {
            $data['title_type']= 'Edit  Form';
            $data['projectinfo'] = $impact_data;


            $data['error_page'] = 'admin/access';
            $data ['meta_keyword']= 'DevHomes,Nigerial,Property Management Service,Dashboard';
            $data['titel'] = 'DevHomes Property Management :- Property Category';
            $data['pageheader'] = "Edit Property";
            $data['breadCrumbs'] = '<li class="breadcrumb-item"><a href="'.site_url("admin/properties").'">Property Table</a></li> <li class="breadcrumb-item active">Edit Property</li>';
            $data['mainmenu'] = "properties";

            $data['state'] =$this->Generic->getAll($tablename='states', $limit=NULL, $fieldlist=null, $createdat=null, $updatedat=null, $orderbyfield='statename');
            $data_wherecity = array(
                        'stateid'    => $impact_data['stateid'], // or $data['clientinfo']['stateid']
                        'status'        =>  1
                    );
            $data['city']= $this->Generic->findByCondition($data_wherecity,'state_cities', $orderbyfield='cityname');
            $data_check = array(  
                                     'userroleid'  => ROLE_LANDLORD,
                                     'status' =>  1
                                     );
            $landlord =  $this->Generic->findByCondition($data_check,'tbl_adminusers');
            $data['landlord'] = $landlord;

            //print("<pre>".print_r($impact_data,true)."</pre>");die;

            $data ['content_file']= 'prop_new';
            $this->load->view('admin/layout', $data);
        }
        else
        {
            redirect('admin/properties');
        }
        
        
    }
    
    
    // Controller fuction to save 
    public function save()
    {
        
            $data_New = array(  
                        'adminuserid'    => $_SESSION['userId'],
                        'name'    => $this->input->post('name'),
                        'description'    => $this->input->post('description'),
                        'type'    => $this->input->post('type'),
                        'name'    => $this->input->post('name'),
                        'bedroom'    => $this->input->post('bedroom'),
                        'bathroom'    => $this->input->post('bathroom'),
                        'toilet'    => $this->input->post('toilet'),
                        'price'    => $this->input->post('price'),
                        'address'    => $this->input->post('address'),
                        'stateid'    => $this->input->post('state'),
                        'cityid'    => $this->input->post('city')
                        );
            // slug library
            $config = array(
                    'table' => 'tbl_property',
                    'id' => 'id',
                    'field' => 'slug',
                    'title' => 'title',
                    'replacement' => 'dash' // Either dash or underscore
                    );
            $this->load->library('slug', $config);

            $data_slug = array(
                    'slug' => $this->input->post("name"),       // slug name of filed in db were ur url is stored
                    );
            $data_New['slug'] = $this->slug->create_uri($data_slug);

            if( $_SESSION['role'] != ROLE_LANDLORD){ 
                $data_New['landlorduserid'] = $this->input->post('landlord');
            }
            else{
                $data_New['landlorduserid'] = $_SESSION['userId'];   
            }

            //print("<pre>".print_r($data_New,true)."</pre>");die;
            // insert to db
            $insert_data = $this->Generic->add($data_New, $tablename="tbl_property"); 

            if($insert_data)
            {
                $impactinfo=$this->Generic->getByFieldSingle('id', $insert_data, $tablename='tbl_property');
                $this->session->set_flashdata('success','success');
                $this->session->set_flashdata('message', 'New Property Added, upload Property Images');
                $Json_resultSave = array ('status' => '1' );
               // echo json_encode($Json_resultSave);
                redirect('admin/properties/gallaryaddform/'.$impactinfo['slug'] );

            }
            else 
            {
                $this->session->set_flashdata('error','error');
                $this->session->set_flashdata('message', 'An error occur when Adding New Property');
                $Json_resultSave = array ('status' => '0' );
                    //echo json_encode($Json_resultSave);
                redirect('admin/properties/addform');
            }
    }
    
    // Controller function to display the add user form
    public function gallaryaddform($slug=null)
    {
        $this->validateadmin();
        $data['error_page'] = 'admin/access';
        $data ['meta_keyword']= 'DevHomes,Nigerial,Property Management Service,Dashboard';
        $data['titel'] = 'DevHomes Property Management :- Property Category';
        $data['pageheader'] = "New Property Gallery";
        $data['breadCrumbs'] = '<li class="breadcrumb-item"><a href="'.site_url("admin/properties").'">Property Table</a></li> <li class="breadcrumb-item active">New Property Images</li>';
        $data['mainmenu'] = "properties";


        $impactinfo=$this->Generic->getByFieldSingle('slug', $slug, $tablename='tbl_property');
        $data['state'] =$this->Generic->getAll($tablename='states', $limit=NULL, $fieldlist=null, $createdat=null, $updatedat=null, $orderbyfield='statename');
        $data_wherecity = array(
                        'stateid'    => $impactinfo['stateid'], // or $data['clientinfo']['stateid']
                        'status'        =>  1
                    );
        $data['city']= $this->Generic->findByCondition($data_wherecity,'state_cities', $orderbyfield='cityname');

        $data_check = array(  
                                 'userroleid'  => ROLE_LANDLORD,
                                 'status' =>  1
                                 );
        $landlord =  $this->Generic->findByCondition($data_check,'tbl_adminusers');
        $data['landlord'] = $landlord;
        $data['impactinfo'] = $impactinfo;

        $data ['content_file']= 'prop_new';
        $this->load->view('admin/layout', $data);
    }
    // Controller function to edit a specified user
    public function edit($id=null)
    {
        $data_New = array(  
                        'adminuserid'   => $_SESSION['userId'],
                        'name'          => $this->input->post('name'),
                        'description'   => $this->input->post('description'),
                        'type'          => $this->input->post('type'),
                        'name'          => $this->input->post('name'),
                        'bedroom'       => $this->input->post('bedroom'),
                        'bathroom'      => $this->input->post('bathroom'),
                        'toilet'        => $this->input->post('toilet'),
                        'price'         => $this->input->post('price'),
                        'address'       => $this->input->post('address'),
                        'stateid'       => $this->input->post('state'),
                        'cityid'        => $this->input->post('city'),
                        'status'        => $this->input->post('status')
                     );
        
        $data_Where = array(  
                        'id'    => $id
                     );
        // slug library
        $config = array(
                'table' => 'tbl_property',
                'id' => 'id',
                'field' => 'slug',
                'title' => 'title',
                'replacement' => 'dash' // Either dash or underscore
                );
        $this->load->library('slug', $config);

        $data_slug = array(
                'slug' => $this->input->post("name"),       // slug name of filed in db were ur url is stored
                );
        $data_New['slug'] = $this->slug->create_uri($data_slug, $id );

        if( $_SESSION['role'] != ROLE_LANDLORD){ 
            $data_New['landlorduserid'] = $this->input->post('landlord');
        }
        else{
            $data_New['landlorduserid'] = $_SESSION['userId'];   
        }
        // insert to db
        $insert_data = $this->Generic->editByConditions($data_New, $data_Where , $tablename="tbl_property"); 

        //print("<pre>".print_r($insert_data,true)."</pre>");die;
        if($insert_data)
            {
                $this->session->set_flashdata('success','Property Info Updated');
                $this->session->set_flashdata('message', 'Property Info Updated');
                $Json_resultSave = array ('status' => '1');
                //echo json_encode($Json_resultSave); exit();

            }
        else {
            $this->session->set_flashdata('error','error');
            $this->session->set_flashdata('message', 'An error occur when Updated Property Info');
            $Json_resultSave = array ('status' => '0');
            //echo json_encode($Json_resultSave); exit();
        }
        redirect('admin/properties');
    }

     public function imageorder()
    {
        $data_New = array(  
                        'order_set' => $_POST["order"]
                     );
        
        $data_Where = array(  
                        'id' => $_POST["id"]
                     );
        // insert to db
        $insert_data = $this->Generic->editByConditions($data_New, $data_Where , $tablename="tbl_gallary"); 

        //print("<pre>".print_r($data_New,true)."</pre>");die;
        if($insert_data)
            {
                //$this->session->set_flashdata('success','Project Info Updated');
                //$this->session->set_flashdata('message', 'Project Gallery Updated');
                $Json_resultSave = array ('status' => '1');
                echo json_encode($Json_resultSave); exit();

            }
        else {
            //$this->session->set_flashdata('error','error');
            //$this->session->set_flashdata('message', 'An error occur when Updated Project Gallery');
            $Json_resultSave = array ('status' => '0');
            echo json_encode($Json_resultSave); exit();
        }
    }

    // Controller function to delete a specified user
    public function delete()
    {
        $by_id = $_POST["_id"];
        //$_data = $this->user->_deleteuser($by_id);

        // delete to db
        $_data =$this->Generic->delete($by_id, $tablename="tbl_property");
        if($_data)
        {
            $this->session->set_flashdata('success','success');
            $this->session->set_flashdata('message', 'Property Deleted');
            $Json_resultSave = array ('status' => '1');
            echo json_encode($Json_resultSave);
            exit();
        }
        else 
        {
            $this->session->set_flashdata('error','error');
            $this->session->set_flashdata('message', 'An error occur when Deleting Property');
            $Json_resultSave = array ('status' => '0');
            echo json_encode($Json_resultSave);
            exit();
        }
    }

    // Controller function to delete a specified user
    public function deleteimg()
    {
        $by_id = $_POST["_id"];
        $picture=$_POST["key"];

        $fullpath = './assets/uploads/';
        $thumbpath = './assets/uploads/thumbs/';
        
        unlink($fullpath.$picture);
        unlink($thumbpath.$picture);
        
        // delete to db
        $_data =$this->Generic->delete($by_id, $tablename="tbl_gallary");
        $this->db->delete('tbl_gallary', array('id'=>$by_id));
        if($_data)
        {
            $this->session->set_flashdata('success','success');
            $this->session->set_flashdata('message', 'Property Image Deleted');
            $Json_resultSave = array ('status' => '1');
            echo json_encode($Json_resultSave);
            exit();
        }
        else 
        {
            $this->session->set_flashdata('error','error');
            $this->session->set_flashdata('message', 'An error occur when Deleting Property Image');
            $Json_resultSave = array ('status' => '0');
            echo json_encode($Json_resultSave);
            exit();
        }
    }

    
}
