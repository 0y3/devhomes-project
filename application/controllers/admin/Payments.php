<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Payments extends CI_Controller {
    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Property');
        $this->load->model('utility');
        $this->load->model('Generic');
        $this->load->helper('text');
    }
    
    public function index()
    {
        //print_r($_SESSION);
        $this->validateadmin();
            
       
        $data['error_page'] = 'admin/access';
        $data ['meta_keyword']= 'DevHomes,Nigerial,Property Management Service,Dashboard';
        $data['titel'] = 'DevHomes Property Management :- Property Category';
        $data['pageheader'] = "Property Payment List";
        $data['breadCrumbs'] = '<li class="breadcrumb-item active">Payment Table</li>';
        $data['mainmenu'] = "payment";
        
        // Load all the impacts
        if($_SESSION['role']==ROLE_USER)
        {
            $data['pay'] = $this->Property->getAllPayment(array('userid_payment'=>$_SESSION['userId']));
        }
        if($_SESSION['role']==ROLE_AGENT)
        {
            $data['pay'] = $this->Property->getAllPayment(array('whouploadid'=>$_SESSION['userId']));
        }
        if($_SESSION['role']==ROLE_LANDLORD)
        {
            $data['pay'] = $this->Property->getAllPayment(array('landlordpropdid'=>$_SESSION['userId']));
        }
        if($_SESSION['role']==ROLE_SUPERADMIN)
        {
            $data['pay'] = $this->Property->getAllPayment();
        }
        //print("<pre>".print_r($this->Property->getAllPayment(array('whouploadid'=>$_SESSION['userId'])),true)."</pre>");die;

        $data ['content_file']= 'pay-list';
        $this->load->view('admin/layout', $data);
        
    }
    
    // Validate a admin user login
    function validateadmin()
    {
        if (! isset ( $_SESSION['isLogIn'] ) || $_SESSION['isLogIn'] != TRUE) 
        {
           redirect('admin');
        }
    }

    public function accessdenied()
    {
        $data['icon'] = 'icon.ico';
        $data ['meta_keyword']= 'DevHomes,Nigerial,Property Management Service,Dashboard';
        $data['titel'] = 'DevHomes Property Management :- Property Category';
        $data['pageheader'] = 'Access Denied';

        $data ['content_file']= 'access';
        $this->load->view('admin/layout', $data);
    }

    // Controller function to approve a specified Payment
    public function approve()
    {
        $this->validateadmin();

        $by_id = $_POST["_id"];

        $paymentinfo = $this->Generic->getByFieldSingle('id', $by_id, $tablename='tbl_payment');

        // edit to db
        $data_edit = array(  
                        'payment'   => 1
                     );
        $data_Where = array(  
                        'id'    => $by_id
                     );
        $_data = $this->Generic->editByConditions($data_edit, $data_Where , $tablename="tbl_payment");


        if($_data)
        {
            $data_edit2 = array(  
                        'status'=> 0 
                     );

            $data_Where2 = array(  
                        'id'    => $paymentinfo['propertyid']
                     );

            $this->Generic->editByConditions($data_edit2, $data_Where2 , $tablename="tbl_property");

            $data_edit2 = array(  
                        'payment'   => 2
                     );
            $data_Where2 = array(  
                            'id !='     => $by_id,
                            'propertyid'=> $paymentinfo['propertyid']
                         );
            $_data = $this->Generic->editByConditions($data_edit2, $data_Where2 , $tablename="tbl_payment");

            //$this->db->delete('tbl_payment', array('propertyid'=> $paymentinfo['propertyid'], 'payment' => 0));

            $this->session->set_flashdata('success','success');
            $this->session->set_flashdata('message', 'Payment Approved');
        }
        else 
        {
            $this->session->set_flashdata('error','error');
            $this->session->set_flashdata('message', 'An error occur when Approveing Payment');
        }
    }

    public function delete()
    {

        $by_id = $_POST["_id"];

        $paymentinfo = $this->Generic->getByFieldSingle('id', $by_id, $tablename='tbl_payment');

        // edit to db
        $data_edit = array(  
                        'payment'   => 2
                     );
        $data_Where = array(  
                        'id'    => $by_id
                     );
        $_data = $this->Generic->editByConditions($data_edit, $data_Where , $tablename="tbl_payment");


        if($_data)
        {
            $this->session->set_flashdata('success','success');
            $this->session->set_flashdata('message', 'Payment Declined');
        }
        else 
        {
            $this->session->set_flashdata('error','error');
            $this->session->set_flashdata('message', 'An error occur when Deleting Payment');
        }
    }

    // Controller function to delete a specified user
    public function delete2()
    {
        $this->validateadmin();

        $by_id = $_POST["_id"];
        //$_data = $this->user->_deleteuser($by_id);

        // delete to db
        $_data = $this->db->delete('tbl_payment', array('id'=>$by_id));//$this->Generic->delete($by_id, $tablename="tbl_adminusers");
        if($_data)
        {
            $this->session->set_flashdata('success','success');
            $this->session->set_flashdata('message', 'Payment Declined');
        }
        else 
        {
            $this->session->set_flashdata('error','error');
            $this->session->set_flashdata('message', 'An error occur when Deleting Payment');
        }
    }

    
}
