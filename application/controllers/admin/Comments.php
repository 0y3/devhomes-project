<?php defined('BASEPATH') OR exit('No direct script access allowed');

class comments extends CI_Controller {
    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Property');
        $this->load->model('utility');
        $this->load->model('Generic');
        $this->load->helper('text');
    }
    
    public function index()
    {
        //print_r($_SESSION);
        $this->validateadmin();
            
       
        $data['error_page'] = 'admin/access';
        $data ['meta_keyword']= 'DevHomes,Nigerial,Property Management Service,Dashboard';
        $data['titel'] = 'DevHomes Property Management :- Property Category';
        $data['pageheader'] = "Property Comments List";
        $data['breadCrumbs'] = '<li class="breadcrumb-item active">Property Comment Table</li>';
        $data['mainmenu'] = "comments";
        
        // Load all the impacts
        $data['pay'] = $this->Property->getAllComment();
        //print("<pre>".print_r($this->Property->getAllComment(),true)."</pre>");die;

        $data ['content_file']= 'comment-list';
        $this->load->view('admin/layout', $data);
        
    }
    
    // Validate a admin user login
    function validateadmin()
    {
        if (! isset ( $_SESSION['isLogIn'] ) || $_SESSION['isLogIn'] != TRUE) 
        {
           redirect('admin');
        }
    }

    public function accessdenied()
    {
        $data['icon'] = 'icon.ico';
        $data ['meta_keyword']= 'DevHomes,Nigerial,Property Management Service,Dashboard';
        $data['titel'] = 'DevHomes Property Management :- Property Category';
        $data['pageheader'] = 'Access Denied';

        $data ['content_file']= 'access';
        $this->load->view('admin/layout', $data);
    }

    // Controller function to approve a specified Payment
    public function approve()
    {
        $this->validateadmin();

        $by_id = $_POST["_id"];

        $commentinfo = $this->Generic->getByFieldSingle('id', $by_id, $tablename='tbl_comment');

        // edit to db
        $data_edit = array(  
                        'status'   => 1
                     );
        $data_Where = array(  
                        'id'    => $by_id
                     );
        $_data = $this->Generic->editByConditions($data_edit, $data_Where , $tablename="tbl_comment");


        if($_data)
        {
            $this->session->set_flashdata('success','success');
            $this->session->set_flashdata('message', 'Payment Approved');
        }
        else 
        {
            $this->session->set_flashdata('error','error');
            $this->session->set_flashdata('message', 'An error occur when Approveing Payment');
        }
    }

    public function decline()
    {

        $by_id = $_POST["_id"];

        $paymentinfo = $this->Generic->getByFieldSingle('id', $by_id, $tablename='tbl_comment');

        // edit to db
        $data_edit = array(  
                        'status' => 0
                     );
        $data_Where = array(  
                        'id'    => $by_id
                     );
        $_data = $this->Generic->editByConditions($data_edit, $data_Where , $tablename="tbl_comment");


        if($_data)
        {
            $this->session->set_flashdata('success','success');
            $this->session->set_flashdata('message', 'Comment Declined');
        }
        else 
        {
            $this->session->set_flashdata('error','error');
            $this->session->set_flashdata('message', 'An error occur when Deleting Comment');
        }
    }

    // Controller function to delete a specified user
    public function delete()
    {
        $this->validateadmin();

        $by_id = $_POST["_id"];
        //$_data = $this->user->_deleteuser($by_id);

        // delete to db
        $_data = $this->db->delete('tbl_comment', array('id'=>$by_id));//$this->Generic->delete($by_id, $tablename="tbl_adminusers");
        if($_data)
        {
            $this->session->set_flashdata('success','success');
            $this->session->set_flashdata('message', 'Comment Declined');
        }
        else 
        {
            $this->session->set_flashdata('error','error');
            $this->session->set_flashdata('message', 'An error occur when Deleting Comment');
        }
    }

    
}
