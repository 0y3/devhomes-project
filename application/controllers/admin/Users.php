<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {
    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('utility');
        $this->load->model('Generic');
        $this->load->model('user');
        $this->load->helper('text');
        $this->load->helper('string');
    }
    
   
    public function index($page=0)
    {
        $this->validateadmin();
        if ( (int)$_SESSION['role'] == ROLE_SUPERADMIN  )
        { 
            //print_r($_SESSION);
           $data ['meta_keyword']= 'DevHomes,Nigerial,Property Management Service,Dashboard';
            $data['titel'] = 'DevHomes Property Management :- Manage Users';
            $data['pageheader'] = "Manage Users";
            $data['breadCrumbs'] = '<li class="breadcrumb-item active">Users</li>';
            $data['mainmenu'] = "user";
            
            $data['error_page'] = 'admin/access';
            
            // Load all the users
            $data['user'] = $this->user->getAllAdmin();
            
            $data ['content_file']= 'user-list';
            $this->load->view('admin/layout', $data);
        }
        else
        {
            $this->accessdenied(); 
        }
    }

    // Validate a admin user login
    function validateadmin()
    {
        if (! isset ( $_SESSION['isLogIn'] ) || $_SESSION['isLogIn'] != TRUE) 
        {
           redirect('admin');
        }
    }

    public function accessdenied()
    {
        $data ['meta_keyword']= 'DevHomes,Nigerial,Property Management Service,Dashboard';
        $data['titel'] = 'DevHomes Property Management:-  Access Denied';
        $data['pageheader'] = 'Access Denied';

        $data ['content_file']= 'access';
        $this->load->view('admin/layout', $data);
    }
    
    // Controller function to display the add user form
    public function addform($id=null)
    {   
        $this->validateadmin();
        if ( ($_SESSION['role'] == ROLE_SUPERADMIN)  )
        
        {
            $data['cate']= $this->user->userroleAdmin();
            $data ['meta_keyword']= 'DevHomes,Nigerial,Property Management Service,Dashboard';
            $data['mainmenu'] = "user";
            
            if(isset($id) && !empty($id))
            {
                if($id == $_SESSION['userId'])
                {
                    redirect('admin/users');
                }
                $data['titel'] = 'DevHomes Property Management:- New Users';
                $userinfo = $this->Generic->getByFieldSingle('id', $id, $tablename='tbl_adminusers');
                if($userinfo==false)
                {
                    redirect('admin/users');
                }
                $data['userinfo']=$userinfo ;
                //print("<pre>".print_r($this->Generic->getByFieldSingle('id', $id, $tablename='admin_users'),true)."</pre>");die;
                $data ['content_file']= 'user_new';
                $data['pageheader'] = "Edit User Form";
                $data['breadCrumbs'] = '<li class="breadcrumb-item"><a href="'.site_url("admin/users").'">Users</a></li> <li class="breadcrumb-item active">Edit User</li>';
            }
            else
            {
                
                $data['titel'] = 'DevHomes Property Management:- New Users';
                $data['pageheader'] = "Add User";
                $data['breadCrumbs'] = '<li class="breadcrumb-item"><a href="'.site_url("admin/users").'">Users</a></li> <li class="breadcrumb-item active">Add User</li>';


                $data ['content_file']= 'user_new';
            }
            $this->load->view('admin/layout', $data);
        }
        else
        {
            $this->accessdenied(); 
        }
    }
    
    // Controller function to add a new user
    public function add()
    {
    }
    
    // Controller fuction to save the user
    public function save()
    {
        // save the new user data  table //
        $encoded_email = urlencode($this->input->post('useremail'));
        $token =  $this->utility->generate_random_string(25);

        $data_check = array(  
                                 'email'  =>  $this->input->post('useremail'),// adding the Encryp name and the extention file 2gether
                                 'isdeleted' =>  0
                                 );
        $check_data =  $this->Generic->findByCondition($data_check,'tbl_adminusers');


        if($check_data)  
        { 
            $this->session->set_flashdata('warning','warning');
            $this->session->set_flashdata('message', 'Email Already Register!!! Try Another Email');
            $Json_resultSave = array (
                                  'status' => '0',
                                  'content' => 'Email Already Register'
                                  );
            echo json_encode($Json_resultSave);
            exit();
        }
        else
        {
            $data_New = array(  
                                    'userroleid'    => $this->input->post('userrole'),
                                    'firstname'     => $this->input->post('firstname'),
                                    'lastname'      => $this->input->post('lastname'),
                                    'email'         => $this->input->post('useremail'),
                                    'phone'         => $this->input->post('phone'),
                                    'email'         => $this->input->post('useremail'),
                                    'status'        => 1
                                 );

            //$insert_data = $this->user->saveuser($data_New);
            
            // insert to db
            $insert_data = $this->Generic->add($data_New, $tablename="tbl_adminusers"); 

            if($insert_data)
            {
                
            $this->session->set_flashdata('success','success');
            $this->session->set_flashdata('message', ' New User Added,');
            $Json_resultSave = array ( 'status' => '1');
            echo json_encode($Json_resultSave);
            exit();
                
            }
            else 
            {
                $this->session->set_flashdata('error','error');
                $this->session->set_flashdata('message', 'An error occur when Adding New User');
                $Json_resultSave = array ( 'status' => '0');
                echo json_encode($Json_resultSave);
                exit();
            }
            redirect('admin/users');
        }
    }
    
    // Controller function to edit a specified user
    public function edit($id)
    {
        $data_New = array(  
                        'userroleid'    => $this->input->post('userrole'),
                        'firstname'     => $this->input->post('firstname'),
                        'lastname'      => $this->input->post('lastname'),
                        'phone'         => $this->input->post('phone'),
                        'status'        => $this->input->post('status')
                     );
        
        $data_Where = array(  
                        'id'    => $id
                     );

        //$insert_data = $this->user->_updateuser($data_New);
        // insert to db
        $insert_data = $this->Generic->editByConditions($data_New, $data_Where , $tablename="tbl_adminusers"); 

        //print("<pre>".print_r($insert_data,true)."</pre>");die;
        if($insert_data)
            {
                $this->session->set_flashdata('success','User Info Updated');
                $this->session->set_flashdata('message', 'User Info Updated');

            }
        else {
            $this->session->set_flashdata('error','error');
            $this->session->set_flashdata('message', 'An error occur when Updated User Info');
        }
        redirect('admin/users');
    }
    // Controller function to delete a specified user
    public function delete()
    {
        $this->validateadmin();

        $by_id = $_POST["_id"];
        //$_data = $this->user->_deleteuser($by_id);

        // delete to db
        $_data = $this->db->delete('tbl_adminusers', array('id'=>$by_id));//$this->Generic->delete($by_id, $tablename="tbl_adminusers");
        if($_data)
        {
            $this->session->set_flashdata('success','success');
            $this->session->set_flashdata('message', 'User Deleted');
        }
        else 
        {
            $this->session->set_flashdata('error','error');
            $this->session->set_flashdata('message', 'An error occur when Deleting User');
        }
    }
    
    









    
}
