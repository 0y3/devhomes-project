<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class : Authentication (LoginController)
 * Authentication class to control to authenticate user credentials and starts user's session.
 * @author : Oye Segun:-- Novo3 Tecch
 * @version : 1.1
 * @since : 15 March 2019
 */
class Authentication extends CI_Controller
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Property');
        $this->load->model('Utility');
        $this->load->model('Generic');
        $this->load->helper('text');
        $this->load->helper('string');
    }

    /**
     * Index Page for this controller.
     */
    public function index()
    {
        $this->login();
    }
    
    /**
     * This function used to check the user is logged in or not
     */
    function login()
    {
        $isLoggedIn = $this->session->userdata('isLogIn');
        
        if(!isset($isLoggedIn) || $isLoggedIn != TRUE )
        {
            $data['icon'] = 'icon.ico';
            $data ['meta_keyword']= 'DevHomes,Nigerial,Property Management Service,Dashboard';
            $data['titel'] = 'DevHomes Property Management:-  Admin Login';
            $this->load->view('admin/login',$data);
        }
        else
        {
            redirect('admin/dashboard');
        }
    }

    /**
     * This function is used to logged out user from system
     */
    function logout() {
        $this->session->sess_destroy ();
        
        redirect ( 'admin' );
    }

    
    
    /**
     * This function used to logged in user
     */
    public function loginMe()
    {
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|max_length[128]|trim');
        $this->form_validation->set_rules('password', 'Password', 'required|max_length[32]');
        
        if($this->form_validation->run() == FALSE)
        {
            $this->index();
        }
        else
        {
            $email = $this->input->post('email');
            $password = $this->input->post('password');

            $result  = $this->Generic->getByFieldSingle('email',$email, $tablename="tbl_adminusers");// get from db
            
            if($result)
            {   
                $getrole  = $this->Generic->getByFieldSingle('id',$result['userroleid'], $tablename="tbl_role");// get role name

                if($result['status']==0)
                {
                    $this->session->set_flashdata('error', 'Email Disactivate!! Contact Admin');
                    redirect('admin/authentication/login');
                }
                if(verifyHashedPassword($password, $result['password']))
                {
                    $sessionArray = array(
                                            'userId'=>$result['id'],                    
                                            'role'=>$result['userroleid'],
                                            'roleText'=>$getrole['rolename'],
                                            'firstname'=>$result['firstname'],
                                            'lastname'=>$result['lastname'],
                                            'isLogIn' => TRUE
                                    );
                                    
                    $this->session->set_userdata($sessionArray);
                    redirect('admin/dashboard');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Email or password mismatch');
                    redirect('admin/authentication/login');
                }
                
            }
            else
            {
                $this->session->set_flashdata('error', 'Email Not Found');
                redirect('admin/authentication/login');
            }
        }
    }

    

    public function get_city_byid()
    {   $by_id = $_POST["stateid"];
        if(isset($by_id) && !empty($by_id) ){

            $data_check = array(  
                                 'stateid'  =>  $by_id
                                 );
            $get_city =  $this->Generic->findByCondition($data_check,'state_cities');  // call City by State

            //print("<pre>".print_r($get_city,true)."</pre>");die;
            foreach($get_city as $row)
            {
                echo '<option value="'.$row['id'].'">'.$row['cityname'].'</option>';
            }
        }
        else{
            
            echo '<option value="">City not available</option>';
        }
    }


    /**
     * This function used to load forgot password view
     */
    public function signup()
    {
        //$encoded_email = urlencode('trivin98@gmail.com');
        //print("<pre>".print_r($encoded_email,true)."</pre>");die;
        $data['icon'] = 'icon.ico';
            $data ['meta_keyword']= 'DevHomes,Nigerial,Property Management Service,Dashboard';
            $data['titel'] = 'DevHomes Property Management:-  Admin signup';
        $this->load->view('admin/signup',$data);
    }
    
    public function save()
    {
        if(empty($this->input->post('userrole')))
        {redirect('admin/authentication/signup');}
    
        $password = $this->input->post('password');
        $cpassword = $this->input->post('cpassword');

        if($password!=$cpassword)
        {
            setFlashData('error', "The Confirm Password field does not match the Password field.");
            redirect('admin/authentication/signup');
        }
        else
        {
            $data_New = array(  
                            'userroleid'    => $this->input->post('userrole'),
                            'firstname'     => $this->input->post('firstname'),
                            'lastname'      => $this->input->post('lastname'),
                            'email'         => $this->input->post('useremail'),
                            'phone'         => $this->input->post('phone'),
                            'email'         => $this->input->post('useremail'),
                            'password'      => getHashedPassword($password),
                            'status'        => 1
                         );
            
            //print("<pre>".print_r($data_New,true)."</pre>");die;
            // insert to db
            $insert_data = $this->Generic->add($data_New, $tablename="tbl_adminusers"); 

            if($insert_data)
            {
                setFlashData('send', ' New User Added,');
                //redirect('admin/authentication/signup');
                
            }
            else 
            {
                setFlashData('error', "An error occur when Adding New User.");
                //redirect('admin/authentication/signup');
            }
            redirect('admin/authentication/signup');
        }
    }
    
    /**
     * This function used to generate reset password request link
     */
    function resetPasswordUser()
    {
        $status = '';
        
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('login_email','Email','trim|required|valid_email|xss_clean');
                
        if($this->form_validation->run() == FALSE)
        {
            $this->forgotPassword();
        }
        else 
        {
            $email = $this->input->post('login_email');
            $check_data= $this->Generic->getByFieldSingle('email',$email, $tablename="tbl_adminusers");

            if($check_data)
            {
                $encoded_email = urlencode($email);
                $activationID =  $this->Utility->generate_random_string(25);

                //print("<pre>".print_r($activationID,true)."</pre>");die;

                $data_New = array(  
                        'userid'        => $check_data['id'],
                        'email'         => $email,
                        'agent'         => getBrowserAgent(),
                        'activation_id' =>$activationID,
                        'client_ip'     => $this->input->ip_address()
                     );

                $insert_data = $this->Generic->add($data_New , $tablename="tbl_restpassword");               
                
                if($insert_data)
                {
                    $data1['reset_link'] = site_url() . "admin/authentication/resetPasswordConfirmUser/".$activationID."/".$encoded_email;
                    $userInfo = $this->Generic->getByFieldSingle('email',$email, $tablename="tbl_adminusers");

                    if(!empty($userInfo)){
                        $data1["name"] = $userInfo['firstname']." ".$userInfo['lastname'];
                        $data1["email"] = $userInfo['email'];
                        $data1["message"] = "Reset Your Password";
                    }

                    $sendStatus = resetPasswordEmail($data1);

                    if($sendStatus){
                        $status = "send";
                        setFlashData($status, "Reset password link sent successfully, please check mails.");
                    } else {
                        $status = $sendStatus;//"notsend";
                        setFlashData($status, "Email has been failed, try again.");
                    }
                }
                else
                {
                    $status = 'unable';
                    setFlashData($status, "It seems an error while sending your details, try again.");
                }
            }
            else
            {
                $status = 'invalid';
                setFlashData($status, "This email is not registered with us.");
            }
            redirect('admin/authentication/forgotPassword');
        }
    }

    // This function used to reset the password 
    function resetPasswordConfirmUser($activation_id, $email)
    {
        // Get email and activation code from URL values at index 3-4
        $email = urldecode($email);
        
        // Check activation id in database
        $data_check = array(  
                                 'email'  =>  $email,
                                 'activation_id' =>  $activation_id
                                 );
        $check_data =  $this->Generic->findByCondition($data_check,'tbl_restpassword');
        
        $data['email'] = $email;
        $data['activation_code'] = $activation_id;
        
        if ($check_data)
        {
            $this->load->view('admin/newPassword', $data);
        }
        else
        {
            redirect('admin/authentication/login');
        }
    }

    // This function used to activate the password 
    function activateConfirmUser($activation_id, $email)
    {
        // Get email and activation code from URL values at index 3-4
        $email = urldecode($email);
        
        // Check activation id in database
        $data_check = array(  
                                 'email'  =>  $email,
                                 'activation_id' =>  $activation_id
                                 );
        $check_data =  $this->Generic->findByCondition($data_check,'tbl_restpassword');
        
        $data['email'] = $email;
        $data['activation_code'] = $activation_id;
        
        if ($check_data)
        {
            $this->load->view('admin/newPassword', $data);
        }
        else
        {
            redirect('admin/authentication/login');
        }
    }
    
    // This function used to create new password
    function createPasswordUser()
    {
        $status = '';
        $message = '';
        $email = $this->input->post("email");
        $activation_id = $this->input->post("activation_code");
        
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('password','Password','required|max_length[20]');
        $this->form_validation->set_rules('cpassword','Confirm Password','trim|required|matches[password]|max_length[20]');
        /*
        if($this->form_validation->run() == FALSE)
        {

            $this->resetPasswordConfirmUser($activation_id, urlencode($email));
        }
        */
            $password = $this->input->post('password');
            $cpassword = $this->input->post('cpassword');

            if($password!=$cpassword)
            {
                $status = 'error';
                $message = 'The Confirm Password field does not match the Password field.';
                setFlashData($status, $message);

                $this->resetPasswordConfirmUser($activation_id, urlencode($email));
            }
            
            // Check activation id in database
            $data_check = array(  
                                 'email'  =>  $email,
                                 'activation_id' =>  $activation_id
                                 );
            $check_data =  $this->Generic->findByCondition($data_check,'tbl_restpassword');
            
            if($check_data)
            {                
                $data_New = array(  
                        'password'        => getHashedPassword($password),
                        'status'          => 1
                        );
        
                $data_Where = array(  
                                'id'    => $check_data[0]['userid']
                             );

                //print("<pre>".print_r($check_data,true)."</pre>");die;
                $insert_data = $this->Generic->editByConditions($data_New, $data_Where , $tablename="tbl_adminusers"); 
                if($insert_data)
                {
                    $this->db->delete('tbl_restpassword', array('email'=>$email));

                    $status = 'success';
                    $message = 'Password changed successfully';
                }
                else
                {
                    $status = 'error';
                    $message = 'Password changed failed, Try Again';
                    $this->resetPasswordConfirmUser($activation_id, urlencode($email));
                }
            }
            else
            {
                $status = 'error';
                $message = 'Password changed failed';
            }
            
            setFlashData($status, $message);

            redirect("admin/authentication/login");

    }

    // This function to SEND Contact Us Details To Admin

    public function postcomment($id)
    {
        $propinfo=$this->Generic->getByFieldSingle('id', $id, $tablename='tbl_property');
        if(empty($propinfo)){redirect('');}
        $data_New = array(      
                          'propertyid'     =>  $id,
                          'name'       =>  ucfirst($this->input->post("commentname")),
                          'email'      =>  $this->input->post("commentemail"),
                          'comment'    =>  $this->input->post("comment"),
                          'admin_read' =>  0,
                          'status'     =>  0
                       );
          //print("<pre>".print_r($data_New,true)."</pre>");die;
        $insert_data = $this->Generic->add($data_New, $tablename="tbl_comment");
          //$this->Restaurant_admin_model->_save_product($data_New, $check);// insert to db
          
        if($insert_data) 
        {

            $this->session->set_flashdata('success','success');
            $this->session->set_flashdata('message', ' Comment Added');
            $Json_resultSave = array ('status' => '1');
        }
        else 
        {
            $this->session->set_flashdata('error','error');
            $this->session->set_flashdata('message', 'An error occur when Adding Comment');
            $Json_resultSave = array ('status' => '0');
        }
        redirect('properties/details/'.$propinfo['slug']);
    }


    function sendcontactEmail($id)
    {

        $propinfo=$this->Generic->getByFieldSingle('id', $id, $tablename='tbl_property');
        $userinfo=$this->Generic->getByFieldSingle('id', $_SESSION['userId'], $tablename='tbl_adminusers');

        if (!isset( $_SESSION['role'] ) || $_SESSION['role'] != ROLE_USER) 
        {
            $this->session->set_flashdata('error','error');
            $this->session->set_flashdata('message', 'Login as User, Please Try Again');
            redirect('properties/details/'.$propinfo['slug']);
        }

        $data_New["customername"]= $userinfo['firstname'].' '.$userinfo['lastname'] ;
        $data_New["email"]      = $userinfo['email'];
        $data_New["phone"]      = $userinfo['phone'];
        $data_New["userid"]     = $_SESSION['userId'];
        $data_New["propertyid"] = $id;

        //print("<pre>".print_r($get_city,true)."</pre>");die
        $sendStatus = $this->Generic->add($data_New, $tablename="tbl_payment"); 
        if($sendStatus)
        {

            $this->session->set_flashdata('success','success');
            $this->session->set_flashdata('message', '<h5>Payment Message Sent</h5>');
        }
        else 
        {
            $this->session->set_flashdata('error','error');
            $this->session->set_flashdata('message', '<h5>An error occur when Sending Your Payment Message,</h5> Please Try Again');
        }
        redirect('properties/details/'.$propinfo['slug']);
    }

    public function loginuser($id)
    {

        $propinfo=$this->Generic->getByFieldSingle('id', $id, $tablename='tbl_property');

            $email = $this->input->post('email');
            $password = $this->input->post('password');

            $result  = $this->Generic->getByFieldSingle('email',$email, $tablename="tbl_adminusers");// get from db
            
            if($result)
            {   
                $getrole  = $this->Generic->getByFieldSingle('id',$result['userroleid'], $tablename="tbl_role");// get role name

                if($result['userroleid'] != 4)
                {
                    $this->session->set_flashdata('error','error');
                    $this->session->set_flashdata('message', '<h5>The Account Login details is not a User Account!!</h5> Try Again');
                    redirect('properties/details/'.$propinfo['slug']);
                }
                if($result['status']==0)
                {
                    $this->session->set_flashdata('error','error');
                    $this->session->set_flashdata('message', '<h5>Email Disactivate!!</h5> Contact Admin');
                    redirect('properties/details/'.$propinfo['slug']);
                }
                if(verifyHashedPassword($password, $result['password']))
                {
                    $sessionArray = array(
                                            'userId'=>$result['id'],                    
                                            'role'=>$result['userroleid'],
                                            'roleText'=>$getrole['rolename'],
                                            'firstname'=>$result['firstname'],
                                            'lastname'=>$result['lastname'],
                                            'isLogIn' => TRUE
                                    );
                                    
                    $this->session->set_userdata($sessionArray);
                    $this->session->set_flashdata('success','success');
                    $this->session->set_flashdata('message', '<h5>Successfully Login!!</h5> Make Payment Now');
                }
                else
                {
                    $this->session->set_flashdata('error','error');
                    $this->session->set_flashdata('message', '<h5>Email or password mismatch</h5>');
                }
                
            }
            else
            {
                $this->session->set_flashdata('error','error');
                $this->session->set_flashdata('message', '<h5>Email Not Found</h5>');

            }

            redirect('properties/details/'.$propinfo['slug']);
    }


}

?>