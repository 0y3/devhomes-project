<?php defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Model
{
        private $DB_user = "tbl_adminusers";
    
	function __construct()
	{
		parent::__construct();
	}

        public function is_user_email_available($data) 
        {
            $this->db->where($data);  
            $query = $this->db->get($this->DB_user);  

            if($query->num_rows() > 0)  
              {  
                return true;  
              }  
            else  
              {  
                return false;  
              }  
        }
        
        public function newtoken($data,$by_id)
        {
            $this->db->set($data); 
            $this->db->where('id', $by_id); //which row want to upgrade  
            $this->db->update($this->DB_user); 
            
        }
        
        public function check_User_pwd($pwd) 
        {
            $query = $this->db->get_where($this->DB_user, array(
                                                            'id' => (int)$this->session->userId,
                                                            'password' => $pwd,
                                                            'isdeleted' =>'0'
                                                                )
                                                            );
            return $query->result();
        }

    function getAllAdmin()
    {
        $this->db->select('tbl_adminusers.*, tbl_role.roleName');
        $this->db->from('tbl_adminusers');
        $this->db->join('tbl_role', 'tbl_adminusers.userroleid = tbl_role.id');
        $this->db->where(array('tbl_adminusers.isdeleted'=>0));

        // Perform checks to show merchants only orders that belong to their store
        
        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else{
            return false;
        }
    }

    function getAdminByID($userID)
    {
            $query = $this->db->get_where('tbl_adminusers', array(
                                                    'id' => (int)$userID,
                                                    'isdeleted' =>'0'
                                                    )
                                         );
            if ($query->num_rows() > 0)
            {
                    $row = $query->row();
                    return $row;
            }
            else
                    return false;
    }

    public function userroleAdmin()
        {
            $this->db->select('*');
            $this->db->from('tbl_role');
            $this->db->where("status ", 1);
            $this->db->order_by("rolename" , "ASC");//DESC

            $query = $this->db->get()->result();

            //print("<pre>".print_r($query,true)."</pre>");die;
            return $query;

        }
	
}

