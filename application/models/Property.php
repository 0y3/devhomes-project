<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Property extends CI_Model
{
    
	function __construct()
	{
		parent::__construct();
	}
	
     function queryParameters($params=array())
    {   
        // filter by property id
        if(isset($params['propertyid'])){
            $this->db->where(array('tbl_property.id'=>$params['propertyid']));
        }

        // filter by Who upload property id
        if(isset($params['whouploadid'])){
            $this->db->where(array('tbl_property.adminuserid'=>$params['whouploadid']));
        }

        // filter by landlord property id
        if(isset($params['landlordpropdid'])){
            $this->db->where(array('tbl_property.landlorduserid'=>$params['landlordpropdid']));
        }
        
        // filter by property slug
        if(isset($params['propertyslug'])){
            $this->db->where(array('tbl_property.slug'=>$params['propertyslug']));
        }

        // filter by property status
        if(isset($params['propertystatus'])){
            $this->db->where(array('tbl_property.status'=>$params['propertystatus']));
        }



        // filter by property slug
        if(isset($params['userid_payment'])){
            $this->db->where(array('tbl_payment.userid'=>$params['userid_payment']));
        }

        // filter by property status
        if(isset($params['propertystatus'])){
            $this->db->where(array('tbl_property.status'=>$params['propertystatus']));
        }



        // filter by property bedroom
        if(isset($params['propertybedroom'])){
            $this->db->where(array('tbl_property.bedroom'=>$params['propertybedroom']));
        }

        // filter by property type
        if(isset($params['propertytype'])){
            $this->db->where(array('tbl_property.type'=>$params['propertytype']));
        }
    }

	function getAll($param=array(), $limit_start=null)
	{
		$this->db->select('tbl_property.*,states.statename,state_cities.cityname,
            tbl_adminusers.firstname,tbl_adminusers.lastname,tbl_adminusers.email,tbl_adminusers.phone');
		$this->db->from('tbl_property');
        $this->db->join('tbl_adminusers', 'tbl_adminusers.id = tbl_property.adminuserid', "left");
        $this->db->join('states', 'states.id = tbl_property.stateid', "left");
        $this->db->join('state_cities', 'state_cities.id = tbl_property.cityid', "left");

        // Process any filter options if any
        $this->queryParameters($param);

		$this->db->where(array(
                            'tbl_property.isdeleted'=>0 
                        )
            );
        $this->db->order_by("tbl_property.createdat", 'DESC');
        
		$query = $this->db->get();
        $get_impact = null;
        if($query->num_rows() > 0)
        {
            //return $query->result_array();
            $rows = $query->result_array();
            foreach($rows as $row)
            {
                $row['photo_count'] =   $this->_get_count($row['id']);
                $row['photo_info'] =   $this->getPhotoById($row['id']);
                $row['comment'] =   $this->getCommentById($row['id']);
                $get_impact[] = $row;
            }
            
            return $get_impact;
        }
        else
        {
            return null;
        }
	}

    Public function getAllCount($param=array(), $limit_start=null)
    {
       $this->db->select('tbl_property.*,states.statename,state_cities.cityname,
            tbl_adminusers.firstname,tbl_adminusers.lastname,tbl_adminusers.email,tbl_adminusers.phone');
        $this->db->from('tbl_property');
        $this->db->join('tbl_adminusers', 'tbl_adminusers.id = tbl_property.adminuserid', "left");
        $this->db->join('states', 'states.id = tbl_property.stateid', "left");
        $this->db->join('state_cities', 'state_cities.id = tbl_property.cityid', "left");

        $this->queryParameters($param);
        // Clause to only fetch data with deletedat field set to null
        $this->db->where(array(
                            'tbl_property.isdeleted'=>0 
                        )
                    );
        $total = $this->db->count_all_results();
        return $total;
    }

    public function _get_count($by_id) 
    {
        $this->db->select("id,name");
        $this->db->from('tbl_gallary');
        $this->db->where("propertyid ", $by_id);
        $this->db->where("status ", 1);
        $this->db->where("isdeleted ", 0);
        $this->db->order_by("order_set", 'ASC');
        $total = $this->db->count_all_results();
        return $total;
        //$query = $this->db->get();
        //return $query->result_array();
    }
    function getPhotoById($id=null)
	{
        $this->db->select("*");
        $this->db->from('tbl_gallary');
        $this->db->where( array(
                                'propertyid' => (int)$id,
                                'status' => 1,
                                'isdeleted' =>0
                                )
                         );
        $this->db->order_by("order_set", 'ASC');
        $this->db->order_by("createdat", 'ASC');
        $query = $this->db->get();
            if ($query->num_rows() > 0)
            {
                    //$row = $query->row();
                    return $query->result_array();
            }
            else
                    return false;
	}

    function getCommentById($id=null)
    {
        $this->db->select("*");
        $this->db->from('tbl_comment');
        $this->db->where( array(
                                'propertyid' => (int)$id,
                                'status' => 1,
                                'isdeleted' =>0
                                )
                         );
        $this->db->order_by("createdat", 'ASC');
        $query = $this->db->get();
            if ($query->num_rows() > 0)
            {
                    //$row = $query->row();
                    return $query->result_array();
            }
            else
                    return false;
    }
       
    function getAllPayment($param=array(), $limit_start=null)
    {
        $this->db->select('tbl_payment.*,tbl_property.name,tbl_property.slug,tbl_property.type,tbl_property.bedroom,tbl_property.bathroom,tbl_property.toilet,tbl_property.price,tbl_property.address,tbl_property.adminuserid,tbl_property.landlorduserid,
            states.statename,state_cities.cityname');
        $this->db->from('tbl_payment');
        $this->db->join('tbl_property', 'tbl_property.id = tbl_payment.propertyid', "left");
        $this->db->join('tbl_adminusers', 'tbl_adminusers.id = tbl_property.adminuserid', "left");
        $this->db->join('states', 'states.id = tbl_property.stateid', "left");
        $this->db->join('state_cities', 'state_cities.id = tbl_property.cityid', "left");

        // Process any filter options if any
        $this->queryParameters($param);

        $this->db->where(array(
                            'tbl_payment.isdeleted'=>0 
                        )
            );

        $this->db->order_by("payment" , "ASC");
        $this->db->order_by("tbl_payment.createdat", 'DESC');
        
        $query = $this->db->get();
        $get_impact = null;
        if($query->num_rows() > 0)
        {
            //return $query->result_array();
            $rows = $query->result_array();
            foreach($rows as $row)
            {
                $row['agentinfo']    =  $this->Generic->getByFieldSingle('id', $row['adminuserid'], $tablename='tbl_adminusers');
                $row['landlordinfo'] =  $this->Generic->getByFieldSingle('id', $row['landlorduserid'], $tablename='tbl_adminusers');
                $get_impact[] = $row;
            }
            
            return $get_impact;
        }
        else
        {
            return null;
        }
    }

    Public function getAllPaymentCount($param=array(), $limit_start=null)
    {
       $this->db->select('tbl_payment.*,tbl_property.name,tbl_property.slug,tbl_property.type,tbl_property.bedroom,tbl_property.bathroom,tbl_property.toilet,tbl_property.price,tbl_property.adsress,tbl_property.adminuserid,
            states.statename,state_cities.cityname');
        $this->db->from('tbl_payment');
        $this->db->join('tbl_property', 'tbl_property.id = tbl_payment.propertyid', "left");
        $this->db->join('tbl_adminusers', 'tbl_adminusers.id = tbl_property.adminuserid', "left");
        $this->db->join('states', 'states.id = tbl_property.stateid', "left");
        $this->db->join('state_cities', 'state_cities.id = tbl_property.cityid', "left");

        $this->queryParameters($param);
        // Clause to only fetch data with deletedat field set to null
        $this->db->where(array(
                            'tbl_payment.isdeleted'=>0 
                        )
                    );
        $total = $this->db->count_all_results();
        return $total;
    }

    function getAllComment($param=array(), $limit_start=null)
    {
        $this->db->select('tbl_comment.*,tbl_property.name as propertyname,tbl_property.slug,tbl_property.type,tbl_property.bedroom,tbl_property.bathroom,tbl_property.toilet,tbl_property.price,tbl_property.address,tbl_property.adminuserid,tbl_property.landlorduserid,
            states.statename,state_cities.cityname');
        $this->db->from('tbl_comment');
        $this->db->join('tbl_property', 'tbl_property.id = tbl_comment.propertyid', "left");
        $this->db->join('tbl_adminusers', 'tbl_adminusers.id = tbl_property.adminuserid', "left");
        $this->db->join('states', 'states.id = tbl_property.stateid', "left");
        $this->db->join('state_cities', 'state_cities.id = tbl_property.cityid', "left");

        // Process any filter options if any
        $this->queryParameters($param);

        $this->db->where(array(
                            'tbl_comment.isdeleted'=>0 
                        )
            );
        $this->db->order_by("tbl_comment.createdat", 'DESC');
        
        $query = $this->db->get();
        $get_impact = null;
        if($query->num_rows() > 0)
        {
            //return $query->result_array();
            $rows = $query->result_array();
            foreach($rows as $row)
            {
                $row['agentinfo']    =  $this->Generic->getByFieldSingle('id', $row['adminuserid'], $tablename='tbl_adminusers');
                $row['landlordinfo'] =  $this->Generic->getByFieldSingle('id', $row['landlorduserid'], $tablename='tbl_adminusers');
                $get_impact[] = $row;
            }
            
            return $get_impact;
        }
        else
        {
            return null;
        }
    }
    Public function getAllCommentCount($param=array(), $limit_start=null)
    {  
        $this->db->select('tbl_comment.*,tbl_property.name as propertyname,tbl_property.slug,tbl_property.type,tbl_property.bedroom,tbl_property.bathroom,tbl_property.toilet,tbl_property.price,tbl_property.address,tbl_property.adminuserid,tbl_property.landlorduserid,
            states.statename,state_cities.cityname');
        $this->db->from('tbl_comment');
        $this->db->join('tbl_property', 'tbl_property.id = tbl_comment.propertyid', "left");
        $this->db->join('tbl_adminusers', 'tbl_adminusers.id = tbl_property.adminuserid', "left");
        $this->db->join('states', 'states.id = tbl_property.stateid', "left");
        $this->db->join('state_cities', 'state_cities.id = tbl_property.cityid', "left");

        $this->queryParameters($param);
        // Clause to only fetch data with deletedat field set to null
        $this->db->where(array(
                            'tbl_comment.isdeleted'=>0 
                        )
                    );
        $total = $this->db->count_all_results();
        return $total;
    }
        
    function getAllAdmin($userid=null)
    {
        $this->db->select('admin_users.*, user_roles.roleName');
        $this->db->from('admin_users');
        $this->db->join('user_roles', 'admin_users.userroleid = user_roles.id');
        $this->db->where(array('admin_users.isdeleted'=>0));

        // Perform checks to show merchants only orders that belong to their store
        
        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else{
            return false;
        }
    }

    public function userroleAdmin()
        {
            $this->db->select('*');
            $this->db->from('user_roles');
            $this->db->where_in("roleFor ", 'ebs');
            $this->db->where("status ", 1);
            $this->db->order_by("createdat" , "DESC");

            $query = $this->db->get()->result();

            //print("<pre>".print_r($query,true)."</pre>");die;
            return $query;

        }
	
}
