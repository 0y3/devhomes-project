<style type="text/css">
.slicknav_menu {
    background: rgba(0, 0, 0, 0.0);
    border-bottom:unset !important;
    border-bottom: 1px solid #585757;
    z-index: 2000;
    position: fixed;
    right: 0;
}
.slicknav_nav
{
    background: rgba(0, 0, 0, 0.6);
}
header {
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    min-width: 320px;
    padding: unset;/* 25px 0;*/
    color: #ffffff;
    background: rgba(0, 0, 0, 0.6);
    z-index: 1000;
    -webkit-transition: all ease-out 0.25s;
    transition: all ease-out 0.25s;
}

header.header-scroll {
    padding: 10px 0;
    background: rgba(0, 0, 0, 0.6);
}

header.header-scroll:hover {
    background: rgba(0, 0, 0, 0.8);
}

/* Logo */
a.site-logo {
    display: block;
    float: left;
    color: #ffffff;
    font-size: 24px;
    height: 34px;
    line-height: 34px;
}

a.site-logo:hover {
    color: #ffffff;
    text-decoration: none;
}

a.site-logo > i {
    font-size: 14px;
    opacity: 0.6;
    -webkit-transition: all ease-in-out 0.25s;
    transition: all ease-in-out 0.25s;
}

a.site-logo:hover > i {
    -webkit-transform: rotate(360deg) scale(1.5);
    transform: rotate(360deg) scale(1.5);
}

/* Menu */
header nav {
    float: right;
}

.site-nav {
    position: fixed;
    top: 0;
    right: -260px;
    bottom: 0;
    overflow-y: auto;
    list-style: none;
    margin: 0;
    padding: 0;
    width: 250px;
    background: #353535;
    z-index: 1001;
    -webkit-transition: -webkit-transform ease-out .3s;
    transition: transform ease-out .3s;
    -webkit-backface-visibility: hidden;
    backface-visibility: hidden;
}

.site-nav.site-nav-visible {
    -webkit-transform: translateX(-260px) translateY(0) translateZ(0);
    transform: translateX(-260px) translateY(0) translateZ(0);
}

.lt-ie10 .site-nav.site-nav-visible {
    right: 0;
}

.site-nav a {
    display: block;
    font-size: 13px;
    color: #ffffff;
    font-weight: 600;
    padding: 0 10px;
    margin: 2px 5px;
    height: 34px;
    line-height: 34px;
    border-radius: 3px;
    text-transform: uppercase;
}

.site-nav a:hover,
.site-nav a:focus {
    color: #333333;
    background: #ffffff;
    text-decoration: none;
}

.site-nav a.active,
.site-nav a.active:hover,
.site-nav a.active:focus,
.site-nav li.active > a {
    color: #ffffff;
    background: rgba(0, 0, 0, 0.4);
}

.site-nav i {
    font-size: 14px;
}

.site-nav i.site-nav-arrow {
    float: right;
    line-height: 34px;
    margin-left: 10px;
}

/* Submenu */
.site-nav ul {
    list-style: none;
    margin: 0;
    padding: 0 0 0 10px;
}

.site-nav ul a {
    color: #dddddd;
    text-transform: none;
    height: auto;
    line-height: normal;
    padding-top: 5px;
    padding-bottom: 5px;
}

/*
=================================================================
(#m03mcs) MAIN CONTENT
=================================================================
*/
/* Media Container */
.media-container {
    position: relative;
    height: 279px;
    overflow: hidden;
}

.media-container > .site-section,.divbg {
    min-height: 279px;
}

.media-container > .site-section,
.media-container > .media-map,
.media-container > .media-image {
    position: absolute;
    top: 0;
    right: 0;
    left: 0;
}

.media-container > .media-imager {
    left: 50%;
    width: 2560px;
    height: 279px;
    margin-left: -1280px;
}

.media-container > .site-section {
    z-index: 200;
    background: rgba(0, 0, 0, 0.2);
    text-shadow: 0 0 30px #d8cdcd;
}
.divbg{
    height: 100%;
    background-repeat: no-repeat;
    /* background-size: 100% 100%; */
    background-position: center;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;
}

.site-section-top {
    padding-top: 114px;
    overflow: hidden;
}

.site-section-light {
    color: #ffffff;
}


/* Parallax */
.parallax-image {
    background-position: 50% 0;
    background-attachment: fixed;
    background-repeat: no-repeat;
    background-size: cover;
}



/* Small devices, Tablets (>767px) */
@media screen and (min-width: 768px) {

    header {
        padding: 5px 0;
    }
}

/* Medium devices, Desktops (>991px) */
@media screen and (min-width: 992px) {

    /* Menu */
    .site-nav {
        position: static;
        top: auto;
        right: auto;
        bottom: auto;
        overflow-y: visible;
        width: auto;
        background: transparent;
        z-index: auto;
        -webkit-transition: none;
        transition: none;
        -webkit-backface-visibility: visible;
        backface-visibility: visible;
    }

    .site-nav.site-nav-visible {
        -webkit-transform: none;
        transform: none;
    }

    .lt-ie10 .site-nav.site-nav-visible {
        right: auto;
    }

    .site-nav li {
        float: left;
        padding: 0;
        margin-left: 10px;
        position: relative;
    }

    .site-nav a {
        margin: 0;
    }

    .site-nav li:hover > ul {
        display: block;
    }

    .site-nav li.active > a {
        border-radius: 3px;
    }

    .site-nav a.site-nav-sub,
    .site-nav li.active:hover > a.site-nav-sub {
        border-bottom-left-radius: 0;
        border-bottom-right-radius: 0;
    }

    .site-nav > li:hover > a,
    .site-nav > li.active:hover > a {
        color: #333333;
        background: #ffffff;
        text-decoration: none;
    }

    .site-nav a.site-nav-sub {
        padding-right: 30px;
    }

    .site-nav i.site-nav-arrow {
        margin: 0 -20px 0 0;
    }

    /* Submenu */
    .site-nav ul {
        display: none;
        position: absolute;
        left: 0;
        top: 34px;
        padding: 2px 0;
        width: 160px;
        background-color: #ffffff;
        border-radius: 3px;
        border-top-left-radius: 0;
        -webkit-box-shadow: 0 2px 2px rgba(0, 0, 0, 0.2);
        box-shadow: 0 2px 2px rgba(0, 0, 0, 0.2);
    }

    .site-nav li:last-child ul {
        left: auto;
        right: 0;
        border-radius: 3px;
        border-top-right-radius: 0;
    }

    .site-nav ul li {
        float: none;
        margin: 0;
        padding: 1px 3px;
    }

    .site-nav ul a {
        color: #333333;
    }

    .site-nav ul a:hover {
        background-color: #eeeeee;
    }

    .site-nav ul a.active,
    .site-nav ul a.active:hover {
        background-color: #454545;
    }
}

</style>
<header class="">
                <div class="container">
                    <!-- Site Logo -->
                     <a href="<?= site_url() ?>">
                            <img src="<?= base_url() ?>assets/img/devhomes.png" alt="logo">
                        </a>
                    <!-- Site Logo -->

                    <!-- Site Navigation -->
                    <!--<nav>
                        
                        <a href="javascript:void(0)" class="btn btn-default site-menu-toggle visible-xs visible-sm">
                            <i class="fa fa-bars"></i>
                        </a>

                        <ul class="site-nav">
                            <li class="visible-xs visible-sm">
                                <a href="javascript:void(0)" class="site-menu-toggle text-center">
                                    <i class="fa fa-times"></i>
                                </a>
                            </li>

                            <li>
                                <a class="sitenav home" href="<?= site_url('')?>">Home</a>
                            </li>
                            <li>
                                <a class="sitenav" target="_blank" href="<?= site_url('admin')?>" class="btn btn-primary">Log In</a>
                            </li>
                            <li>
                                <a class="sitenav" target="_blank" href="<?= site_url('admin')?>" class="btn btn-success">Sign Up</a>
                            </li>
                        </ul>
                    </nav>-->
                    <!-- END Site Navigation -->
                    <nav id="menu" class="d-none d-lg-block">
                            <ul class="text-center">
                                <li class="sitenav home "><a href="<?= site_url('')?>">Home</a></li>
                        <?php if(isset($_SESSION['role']) && $_SESSION['role'] == ROLE_USER): ?>
                                <li class="sitenav"> <?=$_SESSION['firstname'] .' '. $_SESSION['lastname']?> </li>
                                <li class="sitenav"><a class="btn-sm btn-info" target="_blank" href="<?= site_url('admin')?>">Dashboard</a></li>
                                <li class="sitenav"><a class="btn-sm btn-danger" href="<?= site_url('admin/dashboard/logout') ?>"> Logout</a></li>
                        <?php else: ?>
                                <li class="sitenav"><a class="btn-sm btn-info" target="_blank" href="<?= site_url('admin')?>">Login</a></li>
                                <li class="sitenav"><a class="btn-sm btn-success" target="_blank" href="<?= site_url('admin/authentication/signup')?>">Signup</a></li>
                        <?php endif; ?>
                            </ul>
                        </nav>
                </div>
            </header>
            <div class="media-container">
                <!-- Intro -->
                <section class="site-section site-section-lightt">
                    <div class="divbg" style="background-image: url('<?=site_url('assets/img/banner3.jpg')?>');">
                        <div class="site-section-light site-section-top" style=" min-height: 279px; background: #3d535594;">
                            <div class="container text-center">
                                <h1 class="animation-slideDown"><strong>Welcome to our Online Store!</strong></h1>
                                <h2 class="h3 animation-slideUp hidden-xs">Explore over 5.000 products!</h2>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- END Intro -->
            </div>
<!--
        <header class="sm_header">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-sm-12 logo-column text-center">
                        <a href="<?= site_url() ?>">
                            <img src="<?= base_url() ?>assets/img/devhomes.png" alt="logo">
                        </a>
                    </div>
                    <div class="col-12 col-sm-12 pt-5 nav-column clearfix">
                        <nav id="menu" class="d-none d-lg-block">
                            <ul class="text-center">
                                <li class="sitenav home "><a href="<?= site_url('')?>">Home</a></li>
                                <li class="sitenav"><a target="_blank" href="<?= site_url('admin')?>">Login</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </header>    
-->
