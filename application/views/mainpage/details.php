
        <!-- ============================================================== -->
        <!-- Banners  -->
        <!-- ============================================================== -->
<div class="container mt-5">
    <div class="row">
        <div class="hero-slider col-xs-8 col-xs-offset-2">
<?php foreach ($propimg as $propimgs) :?>
            <div class="single-slide small_sliderr" style=" background-size: 100% 100%; background-image: url(<?= site_url('assets/uploads/'.$propimgs['name']) ?> )"></div>
<?php endforeach; ?>
        </div>
    </div>
</div>
        <!-- ============================================================== -->
        <!-- End Banners - -->
        <!-- ============================================================== -->

        <!-- ============================================================== -->
        <!-- Content  -->
        <!-- ============================================================== -->
        <div class="contact-area mt-5">
<?php foreach ($prop as $bizs) :?>
            <div class="container">
              <div class="row">
                  <div class="col-12">
                      <?php $this->load->view('admin/alerts');// include_once "/alerts.php"; ?>
                  </div>
              </div>

                <div class="row">
                    <div class="col-md-10 mb-3 contact-info">
                        <div class="single-info">
                            <h2><?=$bizs['name']?></h2>
                        </div>
                        <div class="single-info">
                            <h5>Description</h5>
                            <p><?=$bizs['description']?></p>
                        </div>
                    </div>
                    <div class="col-md-5 contact-info">
                        <p style="margin-bottom:unset;">Property Type: <b><?=$bizs['type']?></b></p>
                        <p style="margin-bottom:unset;">Bedroom: <b><?=$bizs['bedroom']?></b></p>
                        <p style="margin-bottom:unset;">Bathroom: <b><?=$bizs['bathroom']?></b></p>
                        <p style="margin-bottom:;">Toilets: <b><?=$bizs['toilet']?></b></p>
                        
                        <div class="single-info">
                            <h5>Address</h5>
                            <b><p><?=$bizs['address']?></br>
                            <?=$bizs['cityname']?>,<?=$bizs['statename']?></p></b>
                        </div>
                        <div class="single-info">
                            <h5>Price</h5>
                            <h4 class="text-danger"><b>₦<?=number_format($bizs['price']);?></b></h4>
                        </div>
                    </div>

                    <div class="col-md-7">
                <?php if(isset($_SESSION['role']) && $_SESSION['role'] == ROLE_USER): ?>
                        <form action="<?=site_url('admin/authentication/sendcontactEmail/').''.$bizs['id']?>"  method="post" class="contact-form">
                            <div class="row">
                                <div class="col-lg-12 text-center">
                                    <h5>Interested in this property?</h5>
                                    <h4 class="text-danger"><b>₦<?=number_format($bizs['price']);?></b></h4>
                                    <p>Call: <b><?=$bizs['phone']?></b> or or Email: <b><?=$bizs['email']?></b><br>
                                     Agent Name: <b><?=$bizs['firstname'].' '.$bizs['lastname']?></b></p>
                                </div>
                                <div class="col-lg-12">
                                    Upload Payment <b>Image or Pdf</b>
                                    <input type="file" accept="image/*,application/pdf" required="" name="" placeholder="Upload Payment" style="height: unset;text-indent: unset;">
                                </div>
                                <di\v class="col-lg-12 text-right">
                                    <input class="button" type="submit" value="Upload Payment Receipt">
                                </div>
                            </div>
                        </form>
                <?php else: ?>
                        <form action="<?=site_url('admin/authentication/loginuser/').''.$bizs['id']?>"  method="post" class="contact-form">
                            <div class="row">
                                <div class="col-lg-12 text-center">
                                    <h5>Interested in this property?</h5>
                                    <h4 class="text-danger"><b>₦<?=number_format($bizs['price']);?></b></h4>
                                    <p>Call: <b><?=$bizs['phone']?></b> or or Email: <b><?=$bizs['email']?></b><br>
                                     Agent Name: <b><?=$bizs['firstname'].' '.$bizs['lastname']?></b></p>
                                </div>

                                <div class="col-lg-6">
                                    <input type="email" required="" name="email" placeholder="UserAccount Email">
                                </div>
                                <div class="col-lg-6">
                                    <input type="password" required="" name="password" placeholder="UserAccount Password">
                                </div>
                                <div class="col-lg-12 text-right">
                                    <input class="button" type="submit" value="User Login">
                                </div>
                            </div>
                        </form>
                <?php endif; ?>
                    </div>

                </div>
            </div>

            <div class="container">
<?php if(!empty($bizs['comment'])): ?>
    <?php foreach ($bizs['comment'] as $bizscomment) :?> 
                <h3>Comments</h3>
                <div class="row mt-5">
                    <div class="col-lg-1 col-md-1">
                        <div>
                            <img style=" height: 50px;" src="<?= site_url()?>assets/img/noimage.jpg" class="rounded-circle img-fluid" alt="">
                        </div>
                    </div>
                    <div class="col-lg-11 col-md-11">
                        <div>
                            <h6><?=$bizscomment['name']?></h6>
                            <p>t<?=$bizscomment['comment']?></p>
                        </div>
                    </div>
                </div>
    <?php endforeach; ?>
<?php endif; ?>
                <div class="row mt-5">
                    <form id="prd_form" action="<?=site_url('admin/authentication/postcomment/').''.$bizs['id']?>" method="post" class="form-horizontal form-bordered">
                        <div class="event-description">
                            <h5>Leave A Reply</h5>
                            <div class="row">
                                <div class="col-lg-6 col-md-6">
                                    <div class="form-group">
                                        <input type="text" name="commentname" required="" class="form-control" placeholder="Fullname">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <div class="form-group">
                                        <input type="email" name="commentemail" required="" class="form-control" placeholder="you@email.com">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                
                                <div class="col-lg-12 col-md-12">
                                    <div class="form-group">
                                        <textarea cols="3" class="form-control" placeholder="Your Comment" name="comment" required=""></textarea>
                                    </div>
                                </div>
                            </div>
                             <div>
                                <button type="Summit" class="btn btn-info btn-join" id="proceed"> Post</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
<?php endforeach; ?>
        </div>
        <!-- ============================================================== -->
        <!-- End Contact  -->
        <!-- ============================================================== -->




        <div class="text-center">
            <div class="container">
                <div class="row">
                    <div class="col-12" style="padding: 30px 0;">
                        <img src="<?= site_url()?>assets/img/contact-stripe.png" alt="">
                    </div>
                </div>
            </div>
        </div>
        

       <!-- Active -->
       <script type="text/javascript">
           $(document).ready(function(){
                $('.current-menu-item').removeClass('current-menu-item').addClass('sitenav');
                $('.contact').removeClass('sitenav').addClass('current-menu-item');
           });
       </script>
       
        
