
        
        <!-- ============================================================== -->
        <!-- Banners  -->
        <!-- ============================================================== -->
        <div class="hero-slider">
            <div class="single-slide" style="background-image: url(<?= site_url('assets/img/contact-slide.png') ?> )"></div>
        </div>
        <!-- ============================================================== -->
        <!-- End Banners - -->
        <!-- ============================================================== -->



        <!-- ============================================================== -->
        <!-- Contact  -->
        <!-- ============================================================== -->
        <div class="contact-area sp followbg">
            <div class="container">

              <div class="row">
                  <div class="col-12">
                      <?php $this->load->view('admin/alerts');// include_once "/alerts.php"; ?>
                  </div>
              </div>

                <div class="row">
                    <div class="col-md-5 contact-info">
                        <div class="single-info">
                            <h5>Phone</h5>
                            <p><b>+7084009555</b></p>
                        </div>
                        <div class="single-info">
                            <h5>Email</h5>
                            <p><b>hopeemerge@gmail.com</b></p>
                        </div>
                        <div class="single-info">
                            <h5>Address</h5>
                            <b><p>Magodo, Lagos, Nigeria</br>
                            Hazel Crest, Illinois, USA</p></b>
                        </div>
                        <div class="single-info">
                            <h5>Social</h5>
                            <p>
                                <a target="_blank" href="https://www.twitter.com/hopeemerge" class="fa fa-twitter"></a>
                                <a target="_blank" href="https://www.instagram.com/hopeemerge" class="fa fa-instagram"></a>
                                <a target="_blank" href="javascript:void(0)" class="fa fa-youtube"></a>
                                <!--<a href="#" class="fa fa-linkedin"></a>
                                <a href="#" class="fa fa-facebook"></a>
                                <a href="#" class="fa fa-pinterest"></a>-->
                            </p>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <form action="<?=site_url('admin/authentication/sendcontactEmail')?>"  method="post" class="contact-form">
                            <div class="row">
                                <div class="col-lg-6">
                                    <input type="text" required="" name="name" placeholder="Name*">
                                </div>
                                <div class="col-lg-6">
                                    <input type="email"required="" name="email" placeholder="Email*">
                                </div>
                                 <div class="col-lg-6">
                                    <input type="phone" required="" name="phone" placeholder="Phone*">
                                </div>
                                <div class="col-lg-6">
                                    <input type="text"required="" name="location" placeholder="Location*">
                                </div>
                                <div class="col-lg-12">
                                    <input type="text" required="" name="subject" placeholder="Subject*">
                                </div>
                                <div class="col-lg-12">
                                    <textarea required="" name="message" placeholder="Message*"></textarea>
                                </div>
                                <div class="col-lg-12">
                                    <input class="button" type="submit" value="Send Message">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Contact  -->
        <!-- ============================================================== -->




        <div class="text-center">
            <div class="container">
                <div class="row">
                    <div class="col-12" style="padding: 30px 0;">
                        <img src="<?= site_url()?>assets/img/contact-stripe.png" alt="">
                    </div>
                </div>
            </div>
        </div>
        

       <!-- Active -->
       <script type="text/javascript">
           $(document).ready(function(){
                $('.current-menu-item').removeClass('current-menu-item').addClass('sitenav');
                $('.contact').removeClass('sitenav').addClass('current-menu-item');
           });
       </script>
       
        
