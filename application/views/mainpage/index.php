



       
        

        <!-- ============================================================== -->
        <!-- Our Service  -->
        <!-- ============================================================== -->
        <div class="service-area bg2 sp">
            <div class="container">
                <div class="search-wrapper">
                    <form action="<?= site_url('')?>" method="get">
                        <div class="row">
                            <div class="col-lg-12 col-md-12">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <select class="form-control" name="propertytype" placeholder="Property Type">
                                                    <option></option>
                                                    <option value="Flat">Flat</option>
                                                    <option value="House">House</option>
                                                    <option value="Mini Flat">Mini Flat</option>
                                                    <option value="Bungalow">Bungalow</option>
                                                    <option value="Single Room">Single Room</option>
                                                </select>
                                            </div>
                                            <div class="col-lg-4 col-md-4">
                                                <select class="form-control" name="propertybedroom" placeholder="Bedrooms">
                                                    <option> </option>
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                </select>
                                            </div>
                                            <div class="col-lg-2 col-md-2">
                                                <button class="btn btn-info btn-join">search</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>  

            <div class="container">
                <div class="row">
<?php if(!empty($prop)): ?>
    <?php $count=1; ?>
    <?php foreach ($prop as $props) :?>
                    <div class="col-lg-4 col-md-6 single-service single-portfolio">
                        <div class="inner">
                            <div class="portfolio-img">
                                <img height="200px" src="<?= site_url()?>assets/uploads/<?= $props['photo_info'][0]['name']?>" alt="">
                                <div class="hover-content">
                                    <div>
                                        <a href="<?= site_url('properties/details/'.$props['slug'])?>"  class="button">Full Details...</a>
                                    </div>
                                </div>
                            </div>
                            <div class="title portfolio-content text-left">
                                <a href="<?= site_url('properties/details/'.$props['slug'])?>"><h5 title="<?=$props['name']?>">
                                    <?php
                                            $value = $props['name'];
                                                $limit = '40';
                                                if (strlen($value) > $limit) {
                                                         $trimValues = substr($value, 0, $limit).'...';
                                                          } 
                                                else {
                                                        $trimValues = $value;
                                                  }
                                            //character_limiter($resta['companydesc'],25); 
                                                  echo $trimValues;
                                    ?> </h5></a>
                            </div>
                                <p style="margin-bottom:unset;">Property Type: <b><?=$props['type']?></b></p>
                                <p style="margin-bottom:unset;">Bedroom: <b><?=$props['bedroom']?></b></p>
                                <p style="margin-bottom:unset;">Bathrooms: <b><?=$props['bathroom']?></b></p>
                                <p style="margin-bottom:unset;">Toilets: <b><?=$props['toilet']?></b></p>
                                <p style="margin-bottom:unset;">Location: <b><?=$props['cityname']?>,<?=$props['statename']?></b></p>
                                <p style="margin-bottom:unset;">Price: <b>₦<?=number_format($props['price']);?></b></p>
                        </div>
                    </div>
    <?php $count++; ?>
    <?php endforeach; ?>

<?php else: ?>
                        <div class="col-lg-12 col-md-12 col-sm-12 mb-20">
                            <h1>No Record </h1>
                        </div>
<?php endif; ?>
                    
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- Our Service  -->
        <!-- ============================================================== -->


       


        

        <!-- ============================================================== -->
        <!-- Partner  -->
        <!-- ============================================================== -->
        <div class="brand-area Partnerbg ">
            <div class="container">
                <div class="section-title">
                    <h2 class="colorB">Our Partner & Sponsors</h2>
                </div>
                <div class="partner-slider">
                    <div class="single-brand">
                        <div class="inner">
                            <a href="#">
                                <img src="<?= site_url()?>assets/img/ourpartner-logo1.png" alt="">
                            </a>
                        </div>
                    </div>
                    <div class="single-brand">
                        <div class="inner">
                            <a href="#">
                                <img src="<?= site_url()?>assets/img/ourpartner-logo2.png" alt="">
                            </a>
                        </div>
                    </div>
                    <div class=" single-brand">
                        <div class="inner">
                            <a href="#">
                                <img src="<?= site_url()?>assets/img/ourpartner-logo4.png" alt="">
                            </a>
                        </div>
                    </div>
                    <div class=" single-brand">
                        <div class="inner">
                            <a href="#">
                                <img src="<?= site_url()?>assets/img/ourpartner-logo6.png" alt="">
                            </a>
                        </div>
                    </div>
                    <div class=" single-brand">
                        <div class="inner">
                            <a href="#">
                                <img src="<?= site_url()?>assets/img/ourpartner-logo7.png" alt="">
                            </a>
                        </div>
                    </div>
                    <div class=" single-brand">
                        <div class="inner">
                            <a href="#">
                                <img src="<?= site_url()?>assets/img/ourpartner-logo8.png" alt="">
                            </a>
                        </div>
                    </div>
                    <div class=" single-brand">
                        <div class="inner">
                            <a href="#">
                                <img src="<?= site_url()?>assets/img/ourpartner-logo9.png" alt="">
                            </a>
                        </div>
                    </div>
                    <div class=" single-brand">
                        <div class="inner">
                            <a href="#">
                                <img src="<?= site_url()?>assets/img/ourpartner-logo10.png" alt="">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Partner  -->
        <!-- ============================================================== -->


       <!-- Active -->
       <script type="text/javascript">
           $(document).ready(function(){
                $('.current-menu-item').removeClass('current-menu-item').addClass('sitenav');
                $('.home').removeClass('sitenav').addClass('current-menu-item');
           });
       </script>
       
        
