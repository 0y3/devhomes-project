

    <div class=" col-md-6">
        <div class="card card-primary">
            <form id="changepassword_data" action="<?= site_url('admin/dashboard/changePassword') ?>" method="post" class="form-horizontal form-bordered" enctype="multipart/form-data">

                <div class="col-md-12">
                    <div class="col-md-8 col-md-offset-2 alert alert-danger alert-dismissable get_error" style="display: none;">

                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button"> × </button>

                        <span class="error_msgr_lg"> </span>

                    </div>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label for="save_name">Old Password</label>
                        <input type="password"  id=""  name="oldpwd" class="form-control" placeholder="Old Password" value="" required >
                    </div>
                    <div class="form-group">
                        <label>New Password</label>
                        <input type="password" id="pwd" minlength="6" name="newpwd" class="form-control " placeholder="New Password" value="" required >
                        <small id="message_c" class="invalid-feedback"></small>
                    </div>
                    <div class="form-group">
                        <label>Confirm New Password</label>
                        <input type="password" id="cfmpwd" minlength="6" name="cfmpwd" class="form-control" placeholder="Confirm New Password" value="" required >
                        <small id="message" class="invalid-feedback"></small>
                    </div>
                </div>
                <div class="card-footer">
                  <button type="submit" class="btn btn-success sbmtbtn"><i class="fa fa-floppy-o"></i> &nbsp; Update &nbsp; </button>
                </div>
            </form>
        </div>
    </div>
                               
       
    <script>
    // change Password submit form
    var error_p = true;
    var error_c = true;

    $('#pwd').on('click', function(){
        $('#cfmpwd').val('');
        $('#message').html('');
        $('#cfmpwd').addClass('is-valid').removeClass('is-invalid');
        error_p = false;
    });
    $('#pwd').on('blur', function(){
        if(this.value.length < 6){ // checks the password value length
           $('#message_c').html('You have entered less than 6 characters for password').css('display', 'block');
           $('#pwd').addClass('is-invalid');
           $(this).focus(); // focuses the current field.
           error_c = true; // stops the execution.
        }
        else{ 
            $('#message_c').html('');
            $('#pwd').addClass('is-valid').removeClass('is-invalid');
            error_c = false;
        }
    });


    $('#cfmpwd').on('keyup', function () {

        if ($('#pwd').val() != $('#cfmpwd').val()) 
            {

                $('#message').html('Not Matching').css('display', 'block');
                $('#cfmpwd').addClass('is-invalid');
                error_p =true;
                $('.sbmtbtn').attr({disabled:true});
            } 
        else 
            {

              $('#message').html('');
              $('#cfmpwd').addClass('is-valid').removeClass('is-invalid');
              error_p = false;
              $('.sbmtbtn').attr({disabled:false});
            }
    });
    
    $("#changepassword_data").submit(function (e){
                
        e.preventDefault();

        if(error_p == true || (error_c == true) )
            {
            }
        else 
        { 
            $('.preloader').css("display", "block");
            var method = $(this).attr('method');
            var data = $(this).serialize();
            var url = $(this).attr('action');

            $.ajax({
               url:url,//site_url+'admin/dashboard/changePassword',
               type:method,
               dataType: 'json',
               data:data
            }).done(function(data)
            {
                window.location.reload();
            });
        }
    });
            
</script>