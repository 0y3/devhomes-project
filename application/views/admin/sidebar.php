    <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="<?= site_url('admin/dashboard')?>" class="nav-link <?php if(isset($mainmenu) && $mainmenu=="dashboard") echo ' active';?>">
              <i class="nav-icon fa fa-dashboard"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          <?php if($_SESSION['role'] != ROLE_USER): ?>
          <li class="nav-item">
            <a href="<?= site_url('admin/properties')?>" class="nav-link <?php if(isset($mainmenu) && $mainmenu=="properties") echo ' active';?>">
              <i class="nav-icon fa fa-upload"></i>
              <p>
                Properties
              </p>
            </a>
          </li>
          <?php endif; ?>
          <li class="nav-item">
            <a href="<?= site_url('admin/payments')?>" class="nav-link <?php if(isset($mainmenu) && $mainmenu=="payment") echo ' active';?>">
              <i class="nav-icon fa fa-money"></i>
              <p>
                Payment 
              </p>
            </a>
          </li>

          <?php if($_SESSION['role'] == ROLE_SUPERADMIN): ?>
          <li class="nav-item">
            <a href="<?= site_url('admin/comments')?>" class="nav-link <?php if(isset($mainmenu) && $mainmenu=="comment") echo ' active';?>">
              <i class="nav-icon fa fa-comment"></i>
              <p>
                Comment
              </p>
            </a>
          </li> 
          <li class="nav-item">
            <a href="<?= site_url('admin/users')?>" class="nav-link <?php if(isset($mainmenu) && $mainmenu=="user") echo ' active';?>">
              <i class="nav-icon fa fa-users"></i>
              <p>
                User 
              </p>
            </a>
          </li>
          <?php endif; ?>

          <!--
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-tree"></i>
              <p>
                UI Elements
                <i class="fa fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="pages/UI/general.html" class="nav-link">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>General</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/UI/icons.html" class="nav-link">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>Icons</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/UI/buttons.html" class="nav-link">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>Buttons</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/UI/sliders.html" class="nav-link">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>Sliders</p>
                </a>
              </li>
            </ul>
          </li>
          -->

        </ul>
    </nav>