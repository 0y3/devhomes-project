<div class="container-fluid">
  <div class="row">
          
          <div class="col-lg-12 col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Users Lists</h3>
                <div class="card-tools">
                  <a href="<?=site_url('admin/users/addform')?>" class="btn btn-block btn-info btn-sm">Add New</a>

                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body p-0 table-responsive ">
                <table class="table table-striped table-hover table-bordered">
                  <thead>
                  <tr>
                    <th style="width: 10px">#</th>
                    <th>User ID</th>
                    <th>Firstname</th>
                    <th>Lastname</th>
                    <th>Role</th>
                    <th>Phone</th>
                    <th>Email</th>
                    <th style="width: 100px">Status</th>
                    <th style="width: 300px"> Action</th>
                  </tr>
                  </thead>
                  <tbody>
                  
                      <?php if(!empty($user)): ?>  
                      <?php $count=1; ?>
                          <?php foreach ($user as $users) :?>

                              <?php 
                             
                                $status = "";
                                $label_color = array("", "danger", "warning", "primary", "success", "info");

                                if($users['status']==1)
                                {
                                    $status='<span class="badge bg-success">Active</span>';
                                }
                                elseif($users['status']==0)
                                {
                                    $status='<span class="badge bg-danger">Inactive</span>';
                                }
                                      
                              ?>
                  <tr>
                    <td><?=$count;?>.</td>
                    <td><?=$users['id'];?></td>
                    <td><?=$users['firstname'];?></td>
                    <td><?=$users['lastname'];?></td>
                    <td><?=$users['roleName'];?></td>
                    <td><?=$users['phone'];?></td>
                    <td><span class="badge bg-warning"><b><?=$users['email'];?> </b></span></td>
                    <td><?=$status;?></td>
                    <td>
                      <div class="margin btn-groupp">
                      <?php if($users['id'] != $_SESSION['userId']): ?>
                        <a href="<?=site_url('admin/users/addform/'.$users['id'])?>" class="btn btn-sm btn-default"><i class="fa fa-align-left fa-edit"></i> Edit</a>
                        <a href="javascript:void(0);"  class="btn btn-sm btn-danger user_del" data-get="<?=$users['id']?>"><i class="fa fa-align-right fa-trash"></i> Delete</a>
                      <?php endif; ?>
                      </div>
                    </td>
                    
                  </tr>
                  <?php $count++; ?>
                  <?php endforeach; ?>
                  <?php else: ?>
      
                      <tr>
                          <td class="text-center" colspan="5"><b>No Users Created</b></td>
                      </tr>
                  <?php endif; ?>
                </tbody>

                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- ./col -->
  </div>
  <!-- /.row -->
</div>

<!-- Modal confirm order  -->
    <div class="modal" id="empty_confirmModal" style="display: none; ">
            <div class="modal-dialog">
                    <div class="modal-content">
                        
                            <div class="modal-body" >
                                <div class="col-sm-12 alert alert-danger" id="empty_confirmMessage"> </div>
                            </div>
                        
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger" id="empty_confirmOk">Ok</button>
                                <button type="button" class="btn btn-success" id="empty_confirmCancel">Cancel</button>
                            </div>
                        
                    </div>
            </div>
    </div>

<script >
  $(document).on("click",".send_email", function(e){
    $('.preloader').css("display", "block");
  });
    
     //  the status process button
    $(document).on("click",".user_del", function(e){
        e.preventDefault();
        var empty_msg = "Are you sure you want to Delete this User?";
        var row_id = $(this).data('get'); // gets value
        
        confirmDialog(empty_msg, function(){
            
            
            $.ajax({
                type:'POST',
                url:'<?= site_url('admin/users/delete')?>',
                dataType: 'json',
                data:{
                        _id:row_id
                    },
                beforeSend: function(){
                       // Show image container
                       $('.user_del').prop("disabled", true);
                       $('.preloader').css("display", "block");
                   },
                success:function(html){
                
                    if(html.status == '1')
                    {
                        
                    }
                    else{  }
                },
                complete:function(data){
                        // Hide image container
                        window.location.reload(); 
                        $('.user_del').prop("disabled", false);
                        
                       }

            });
        
        }); 
        
    }); 
    
    function confirmDialog(message, onConfirm){
          var fClose = function(){
          modal.modal("hide");
          };
          var modal = $("#empty_confirmModal");
          modal.modal("show");
          $("#empty_confirmMessage").empty().append(message);
          $("#empty_confirmOk").unbind().one('click', onConfirm).one('click', fClose);
          $("#empty_confirmCancel").unbind().one("click", fClose);
        }
        
    
    
</script>