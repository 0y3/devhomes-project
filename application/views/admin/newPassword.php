<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>CodeInsect | Admin System Log in</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

    <link href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
      .valid-feedback {
        display: none;
        width: 100%;
        margin-top: 0.25rem;
        font-size: 80%;
        color: #28a745;
      }

      .valid-tooltip {
        position: absolute;
        top: 100%;
        z-index: 5;
        display: none;
        max-width: 100%;
        padding: .5rem;
        margin-top: .1rem;
        font-size: .875rem;
        line-height: 1;
        color: #ffffff;
        background-color: rgba(40, 167, 69, 0.8);
        border-radius: .2rem;
      }

      .was-validated .form-control:valid, .form-control.is-valid, .was-validated
      .custom-select:valid,
      .custom-select.is-valid {
        border-color: #28a745;
      }

      .was-validated .form-control:valid:focus, .form-control.is-valid:focus, .was-validated
      .custom-select:valid:focus,
      .custom-select.is-valid:focus {
        border-color: #28a745;
        box-shadow: 0 0 0 0.2rem rgba(40, 167, 69, 0.25);
      }

      .was-validated .form-control:valid ~ .valid-feedback,
      .was-validated .form-control:valid ~ .valid-tooltip, .form-control.is-valid ~ .valid-feedback,
      .form-control.is-valid ~ .valid-tooltip, .was-validated
      .custom-select:valid ~ .valid-feedback,
      .was-validated
      .custom-select:valid ~ .valid-tooltip,
      .custom-select.is-valid ~ .valid-feedback,
      .custom-select.is-valid ~ .valid-tooltip {
        display: block;
      }

      .was-validated .form-check-input:valid ~ .form-check-label, .form-check-input.is-valid ~ .form-check-label {
        color: #28a745;
      }

      .was-validated .form-check-input:valid ~ .valid-feedback,
      .was-validated .form-check-input:valid ~ .valid-tooltip, .form-check-input.is-valid ~ .valid-feedback,
      .form-check-input.is-valid ~ .valid-tooltip {
        display: block;
      }

      .was-validated .custom-control-input:valid ~ .custom-control-label, .custom-control-input.is-valid ~ .custom-control-label {
        color: #28a745;
      }

      .was-validated .custom-control-input:valid ~ .custom-control-label::before, .custom-control-input.is-valid ~ .custom-control-label::before {
        background-color: #71dd8a;
      }

      .was-validated .custom-control-input:valid ~ .valid-feedback,
      .was-validated .custom-control-input:valid ~ .valid-tooltip, .custom-control-input.is-valid ~ .valid-feedback,
      .custom-control-input.is-valid ~ .valid-tooltip {
        display: block;
      }

      .was-validated .custom-control-input:valid:checked ~ .custom-control-label::before, .custom-control-input.is-valid:checked ~ .custom-control-label::before {
        background-color: #34ce57;
      }

      .was-validated .custom-control-input:valid:focus ~ .custom-control-label::before, .custom-control-input.is-valid:focus ~ .custom-control-label::before {
        box-shadow: 0 0 0 1px #ffffff, 0 0 0 0.2rem rgba(40, 167, 69, 0.25);
      }

      .was-validated .custom-file-input:valid ~ .custom-file-label, .custom-file-input.is-valid ~ .custom-file-label {
        border-color: #28a745;
      }

      .was-validated .custom-file-input:valid ~ .custom-file-label::before, .custom-file-input.is-valid ~ .custom-file-label::before {
        border-color: inherit;
      }

      .was-validated .custom-file-input:valid ~ .valid-feedback,
      .was-validated .custom-file-input:valid ~ .valid-tooltip, .custom-file-input.is-valid ~ .valid-feedback,
      .custom-file-input.is-valid ~ .valid-tooltip {
        display: block;
      }

      .was-validated .custom-file-input:valid:focus ~ .custom-file-label, .custom-file-input.is-valid:focus ~ .custom-file-label {
        box-shadow: 0 0 0 0.2rem rgba(40, 167, 69, 0.25);
      }

      .invalid-feedback {
        display: none;
        width: 100%;
        margin-top: 0.25rem;
        font-size: 80%;
        color: #dc3545;
      }

      .invalid-tooltip {
        position: absolute;
        top: 100%;
        z-index: 5;
        display: none;
        max-width: 100%;
        padding: .5rem;
        margin-top: .1rem;
        font-size: .875rem;
        line-height: 1;
        color: #ffffff;
        background-color: rgba(220, 53, 69, 0.8);
        border-radius: .2rem;
      }

      .was-validated .form-control:invalid, .form-control.is-invalid, .was-validated
      .custom-select:invalid,
      .custom-select.is-invalid {
        border-color: #dc3545;
      }

      .was-validated .form-control:invalid:focus, .form-control.is-invalid:focus, .was-validated
      .custom-select:invalid:focus,
      .custom-select.is-invalid:focus {
        border-color: #dc3545;
        box-shadow: 0 0 0 0.2rem rgba(220, 53, 69, 0.25);
      }

      .was-validated .form-control:invalid ~ .invalid-feedback,
      .was-validated .form-control:invalid ~ .invalid-tooltip, .form-control.is-invalid ~ .invalid-feedback,
      .form-control.is-invalid ~ .invalid-tooltip, .was-validated
      .custom-select:invalid ~ .invalid-feedback,
      .was-validated
      .custom-select:invalid ~ .invalid-tooltip,
      .custom-select.is-invalid ~ .invalid-feedback,
      .custom-select.is-invalid ~ .invalid-tooltip {
        display: block;
      }

      .was-validated .form-check-input:invalid ~ .form-check-label, .form-check-input.is-invalid ~ .form-check-label {
        color: #dc3545;
      }

      .was-validated .form-check-input:invalid ~ .invalid-feedback,
      .was-validated .form-check-input:invalid ~ .invalid-tooltip, .form-check-input.is-invalid ~ .invalid-feedback,
      .form-check-input.is-invalid ~ .invalid-tooltip {
        display: block;
      }

      .was-validated .custom-control-input:invalid ~ .custom-control-label, .custom-control-input.is-invalid ~ .custom-control-label {
        color: #dc3545;
      }

      .was-validated .custom-control-input:invalid ~ .custom-control-label::before, .custom-control-input.is-invalid ~ .custom-control-label::before {
        background-color: #efa2a9;
      }

      .was-validated .custom-control-input:invalid ~ .invalid-feedback,
      .was-validated .custom-control-input:invalid ~ .invalid-tooltip, .custom-control-input.is-invalid ~ .invalid-feedback,
      .custom-control-input.is-invalid ~ .invalid-tooltip {
        display: block;
      }

      .was-validated .custom-control-input:invalid:checked ~ .custom-control-label::before, .custom-control-input.is-invalid:checked ~ .custom-control-label::before {
        background-color: #e4606d;
      }

      .was-validated .custom-control-input:invalid:focus ~ .custom-control-label::before, .custom-control-input.is-invalid:focus ~ .custom-control-label::before {
        box-shadow: 0 0 0 1px #ffffff, 0 0 0 0.2rem rgba(220, 53, 69, 0.25);
      }

      .was-validated .custom-file-input:invalid ~ .custom-file-label, .custom-file-input.is-invalid ~ .custom-file-label {
        border-color: #dc3545;
      }

      .was-validated .custom-file-input:invalid ~ .custom-file-label::before, .custom-file-input.is-invalid ~ .custom-file-label::before {
        border-color: inherit;
      }

      .was-validated .custom-file-input:invalid ~ .invalid-feedback,
      .was-validated .custom-file-input:invalid ~ .invalid-tooltip, .custom-file-input.is-invalid ~ .invalid-feedback,
      .custom-file-input.is-invalid ~ .invalid-tooltip {
        display: block;
      }

      .was-validated .custom-file-input:invalid:focus ~ .custom-file-label, .custom-file-input.is-invalid:focus ~ .custom-file-label {
        box-shadow: 0 0 0 0.2rem rgba(220, 53, 69, 0.25);
      }
    </style>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="login-page">
    <div class="login-box">
      <div class="login-logo">
        <a href="<?= site_url() ?>">
          <img class="img-responsive" src="<?= base_url() ?>assets/img/hope-logo.png" alt="logo">
        </a>
      </div><!-- /.login-logo -->
      <div class="login-box-body">
        <p class="login-box-msg">Reset Password</p>
        <?php $this->load->helper('form'); ?>
        <div class="row">
            <div class="col-md-12">
                <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
            </div>
        </div>
        <?php
        $this->load->helper('form');
        $error = $this->session->flashdata('error');
        if($error)
        {
            ?>
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $this->session->flashdata('error'); ?>                    
            </div>
        <?php } ?>
        
        <form action="<?= base_url(); ?>admin/authentication/createPasswordUser" method="post">
          <div class="form-group has-feedback">
            <input type="email" class="form-control" placeholder="Email" name="email" value="<?php echo $email; ?>" readonly required />
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            <input type="hidden" name="activation_code"  value="<?php echo $activation_code; ?>" required />
          </div>
          <hr>
          <div class="form-group has-feedback">
            <input type="password" id="pwd" class="form-control" placeholder="Password" name="password" required />
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            <small id="message_c" class="invalid-feedback"></small>
          </div>
          <div class="form-group has-feedback">
            <input type="password" id="cfmpwd" class="form-control" placeholder="Confirm Password" name="cpassword" required />
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            <small id="message" class="invalid-feedback"></small>
          </div>
          <div class="row">
            <div class="col-xs-8">    
              <!-- <div class="checkbox icheck">
                <label>
                  <input type="checkbox"> Remember Me
                </label>
              </div>  -->                       
            </div><!-- /.col -->
            <div class="col-xs-4">
              <input type="submit" class="btn btn-primary btn-block btn-flat sbmtbtn" value="Submit" />
            </div><!-- /.col -->
          </div>
        </form>
        
      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->

    <script src="<?php echo base_url(); ?>assets/js/jquery-2.1.4.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script>
      // change Password submit form
      var error_p = true;
      var error_c = true;

      $('#pwd').on('click', function(){
          $('#cfmpwd').val('');
          $('#message').html('');
          $('#cfmpwd').addClass('is-valid').removeClass('is-invalid');
          error_p = false;
      });
      $('#pwd').on('blur', function(){
          if(this.value.length < 6){ // checks the password value length
             $('#message_c').html('You have entered less than 6 characters for password').css('display', 'block');
             $('#pwd').addClass('is-invalid');
             $(this).focus(); // focuses the current field.
             error_c = true; // stops the execution.
          }
          else{ 
              $('#message_c').html('');
              $('#pwd').addClass('is-valid').removeClass('is-invalid');
              error_c = false;
          }
      });


      $('#cfmpwd').on('keyup', function () {

          if ($('#pwd').val() != $('#cfmpwd').val()) 
              {

                  $('#message').html('Not Matching').css('display', 'block');
                  $('#cfmpwd').addClass('is-invalid');
                  error_p =true;
                  $('.sbmtbtn').attr({disabled:true});
              } 
          else 
              {

                $('#message').html('');
                $('#cfmpwd').addClass('is-valid').removeClass('is-invalid');
                error_p = false;
                $('.sbmtbtn').attr({disabled:false});
              }
      });
      
      $("#changepassword_data").submit(function (e){
                  
          e.preventDefault();

          if(error_p == true || (error_c == true) )
              {
              }
          else 
          { 
              $('.preloader').css("display", "block");
              var method = $(this).attr('method');
              var data = $(this).serialize();
              var url = $(this).attr('action');

              $.ajax({
                 url:url,//site_url+'admin/dashboard/changePassword',
                 type:method,
                 dataType: 'json',
                 data:data
              }).done(function(data)
              {
                  window.location.reload();
              });
          }
      });
              
  </script>
  </body>
</html>

