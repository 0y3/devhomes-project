

    <div class=" col-md-12">
                <?php
                    if(isset($projectinfo))
                    { 
                        $action='edit/'.$projectinfo['id'];
                        $savename='Update';
                    }
                    else
                    { 
                        $action='save';
                        $savename='Save';

                    }
                ?>
        <div class="card card-default">
            <form id="" action="<?= site_url('admin/properties/'.$action) ?>" method="post" class="form-horizontal form-bordered" enctype="multipart/form-data">
                <div class="card-header">
                    <h3 class="card-title">Project Details</h3>
                </div>
                <div class="col-md-12">
                    <div class="col-md-8 col-md-offset-2 alert alert-danger alert-dismissable get_error" style="display: none;">

                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button"> × </button>

                        <span class="error_msgr_lg"> </span>

                    </div>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label for="save_name">Name</label>
                        <input type="text" id=""  name="name" required="" class="form-control" placeholder="Enter Project Name" value="<?php if(isset($impactinfo)) echo $impactinfo['name']?><?php if(isset($projectinfo)) echo $projectinfo['name']?>" >
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-7">
                                <label>Short Description</label>
                                <textarea class="form-control" name="description" rows="2" placeholder="Enter Property Short Description"><?php if(isset($impactinfo)) echo $impactinfo['description']?><?php if(isset($projectinfo)) echo $projectinfo['description']?></textarea>
                            </div>
                            <div class="col-lg-5">
                                <label>Price</label>
                                <input type="number" name="price" class="form-control" required="" placeholder="Price of Property" value="<?php if(isset($impactinfo)) echo $impactinfo['price']?><?php if(isset($projectinfo)) echo $projectinfo['price']?>">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-3">
                                <label>Property Type</label>
                                <select class="form-control" name="type" required="" placeholder="Property Type">
                                    <option></option>
                                    <option value="Flat" <?php if(isset($impactinfo) && $impactinfo['type']=='Flat' ) echo 'selected'?> <?php if(isset($projectinfo) && $projectinfo['type']=='Flat' ) echo 'selected'?> >Flat</option>
                                    <option value="House" <?php if(isset($impactinfo) && $impactinfo['type']=='House' ) echo 'selected'?> <?php if(isset($projectinfo) && $projectinfo['type']=='House' ) echo 'selected'?>>House</option>
                                    <option value="Mini Flat" <?php if(isset($impactinfo) && $impactinfo['type']=='Mini Flat' ) echo 'selected'?> <?php if(isset($projectinfo) && $projectinfo['type']=='Mini Flat' ) echo 'selected'?>>Mini Flat</option>
                                    <option value="Bungalow" <?php if(isset($impactinfo) && $impactinfo['type']=='Bungalow' ) echo 'selected'?> <?php if(isset($projectinfo) && $projectinfo['type']=='Bungalow' ) echo 'selected'?>>Bungalow</option>
                                    <option value="Single Room" <?php if(isset($impactinfo) && $impactinfo['type']=='Single Room' ) echo 'selected'?> <?php if(isset($projectinfo) && $projectinfo['type']=='Single Room' ) echo 'selected'?>>Single Room</option>
                                </select>
                            </div>
                            <div class="col-lg-3 ">
                                <label>Bedrooms</label>
                                <input type="number" name="bedroom" class="form-control" required="" placeholder="" value="<?php if(isset($impactinfo)) echo $impactinfo['bedroom']?><?php if(isset($projectinfo)) echo $projectinfo['bedroom']?>">
                            </div>
                            <div class="col-lg-3">
                                <label>Bathroom</label>
                                <input type="number" name="bathroom" class="form-control" required="" placeholder="No of Bathroom" value="<?php if(isset($impactinfo)) echo $impactinfo['bathroom']?><?php if(isset($projectinfo)) echo $projectinfo['bathroom']?>">
                            </div>
                            <div class="col-lg-3 ">
                                <label>Toilets</label>
                                <input type="number" name="toilet" class="form-control" required="" placeholder="No of Toilet" value="<?php if(isset($impactinfo)) echo $impactinfo['toilet']?><?php if(isset($projectinfo)) echo $projectinfo['toilet']?>">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-6">
                                <label>Address</label>
                                <textarea class="form-control" name="address" rows="2" placeholder="Enter Address"><?php if(isset($impactinfo)) echo $impactinfo['address']?><?php if(isset($projectinfo)) echo $projectinfo['address']?></textarea>
                            </div>
                            <div class="col-md-3">
                                <label class="control-label">State</label>
                                <select id="state_div" class="form-control select2 "name="state" required="" data-placeholder="Select State..." value="<?php if(isset($impactinfo)) echo $impactinfo['stateid']?><?php if(isset($projectinfo)) echo $projectinfo['stateid']?>">
                                    
                                    <?php if(!empty($state)): ?>
                                        <option></option>
                                        
                                        <?php foreach ($state as $category_list) :?>
                                            <?php if(isset($projectinfo)): ?>
                                                <?php if($projectinfo['stateid'] == $category_list['id']): ?>
                                                    <option value="<?= $category_list['id'] ?>" selected="" ><?= $category_list['statename'] ?></option>
                                                    <?php else: ?>
                                                    <option value="<?= $category_list['id'] ?>" ><?= $category_list['statename'] ?></option>
                                                <?php endif; ?>
                                            <?php elseif(isset($impactinfo)): ?>
                                                <?php if($impactinfo['stateid'] == $category_list['id']): ?>
                                                    <option value="<?= $category_list['id'] ?>" selected="" ><?= $category_list['statename'] ?></option>
                                                    <?php else: ?>
                                                    <option value="<?= $category_list['id'] ?>" ><?= $category_list['statename'] ?></option>
                                                <?php endif; ?>
                                            <?php else: ?>
                                                <option value="<?= $category_list['id'] ?>"><?= $category_list['statename'] ?></option>
                                            <?php endif; ?>
                                        <?php endforeach;?>
                                    <?php else: ?>
                    
                                        <option ></option>

                                    <?php endif; ?>

                                </select> 
                          </div>
                          <div class="col-md-3">
                              <label class="control-label">City</label>
                              <select id="city_div" name="city" class="form-control select-search" data-placeholder="City" required="" value="<?php if(isset($impactinfo)) echo $impactinfo['cityid']?><?php if(isset($projectinfo)) echo $projectinfo['cityid']?>">
                                <?php if(!empty($city)): ?>
                                  <option>Choose A State First..</option>
                                  
                                  <?php foreach ($city as $category_list) :?>
                                      <?php if(isset($projectinfo)): ?>
                                          <?php if($projectinfo['cityid'] == $category_list['id']): ?>
                                              <option value="<?= $category_list['id'] ?>" selected="" ><?= $category_list['cityname'] ?></option>
                                              <?php else: ?>
                                              <option value="<?= $category_list['id'] ?>" ><?= $category_list['cityname'] ?></option>
                                          <?php endif; ?>
                                      <?php elseif(isset($impactinfo)): ?>
                                          <?php if($impactinfo['cityid'] == $category_list['id']): ?>
                                              <option value="<?= $category_list['id'] ?>" selected="" ><?= $category_list['cityname'] ?></option>
                                              <?php else: ?>
                                              <option value="<?= $category_list['id'] ?>" ><?= $category_list['cityname'] ?></option>
                                          <?php endif; ?>
                                      <?php else: ?>
                                          <option value="<?= $category_list['id'] ?>"><?= $category_list['cityname'] ?></option>
                                      <?php endif; ?>
                                  <?php endforeach;?>
                              <?php else: ?>
              
                                  <option>Choose A State First..</option>

                              <?php endif; ?>
                              </select> 
                          </div>

                        </div>
                    </div>
<?php if($_SESSION['role'] != ROLE_LANDLORD): ?> 
                    <div class="form-group">
                        <label class="control-label">Property Landlord</label>

                        <select name="landlord" class="form-control select-search" data-placeholder="City" required="" value="<?php if(isset($impactinfo)) echo $impactinfo['landlorduserid']?><?php if(isset($projectinfo)) echo $projectinfo['landlorduserid']?>">
                                <?php if(!empty($landlord)): ?>
                                  <option>Choose A Landlord For The Property...</option>
                                  
                                  <?php foreach ($landlord as $category_list) :?>
                                      <?php if(isset($projectinfo)): ?>
                                          <?php if($projectinfo['landlorduserid'] == $category_list['id']): ?>
                                              <option value="<?= $category_list['id'] ?>" selected="" ><?= $category_list['firstname'].' '.$category_list['lastname'] ?></option>
                                              <?php else: ?>
                                              <option value="<?= $category_list['id'] ?>" ><?= $category_list['firstname'].' '.$category_list['lastname'] ?></option>
                                          <?php endif; ?>
                                      <?php elseif(isset($impactinfo)): ?>
                                          <?php if($impactinfo['landlorduserid'] == $category_list['id']): ?>
                                              <option value="<?= $category_list['id'] ?>" selected="" ><?= $category_list['firstname'].' '.$category_list['lastname'] ?></option>
                                              <?php else: ?>
                                              <option value="<?= $category_list['id'] ?>" ><?= $category_list['firstname'].' '.$category_list['lastname'] ?></option>
                                          <?php endif; ?>
                                      <?php else: ?>
                                          <option value="<?= $category_list['id'] ?>"><?= $category_list['firstname'].' '.$category_list['lastname'] ?></option>
                                      <?php endif; ?>
                                  <?php endforeach;?>
                              <?php else: ?>
              
                                   <option>Choose A Landlord For The Property...</option>

                              <?php endif; ?>
                              </select> 

                    </div>
<?php endif; ?>               
                    
                    
                    <?php if(isset($impactinfo)): ?>
                    <div class="gallery">
                        <div class="card-header">
                            <h3 class="card-title">Property Gallery</h3>
                        </div>
                        <div id="WaitMe_photo" class="form-group">
                            
                            <div class="">
                                <a class="btn" href="javascript:void(0)" onclick="$('#pro-image').click()"><i class="fa fa-upload"></i> Upload Image</a>
                                <input type="file" id="pro-image" name="pro-image[]" style="display: none;" class="form-control" multiple>
                            </div>
                            
                            <div class="preview-images-zone">
                                
                               
                                
                            </div>
                            
                        </div>
                    </div>
                    <?php endif; ?>
                    <?php if(isset($projectinfo)): ?>
                    <div class="form-group">

                        
                        <div class="col-sm-6">
                            <label>Status</label>
                            <br>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="customRadioInline1" name="status" value="1" class="custom-control-input" required="" <?php if($projectinfo['status']==1) echo 'checked';?> >
                                <label class="custom-control-label  text-success" for="customRadioInline1">Active</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="customRadioInline2" name="status"  required=""  <?php if($projectinfo['status']==0) echo 'checked';?> value="0" class="custom-control-input">
                                <label class="custom-control-label text-danger" for="customRadioInline2">In-Active</label>
                            </div>
                        </div>
                        

                    </div>
                    <?php endif; ?>
                </div>
                <?php if(!isset($impactinfo)): ?>
                <div class="card-footer">
                  <button type="submit" class="btn btn-success sbmtbtnn"><i class="fa fa-floppy-o"></i> &nbsp; <?= $savename?> &nbsp; </button>
                </div>
                <?php endif; ?>
            </form>
        </div>
    </div>
        

    <script>

      

            var reader; 
            function GetUpload(input) {
                if (input.files && input.files[0]) {
                     reader = new FileReader();
                    reader.onload = function (e) {
                        $('.UploadView')
                                .attr('src', e.target.result)
                                //.width(200)
                                //.height(200)
                    };
                    reader.readAsDataURL(input.files[0]);
                }
            }
            
            function RemoveUpload() {
                
                $('.UploadView').attr('src', '');
                $('#fake-input').val('Choose your Image');
                $('.Prd_UploadView').hide();
            }
           
            $('.div_reset').on('click','.btn_reset', function(e){
            //$('.div_reset').click(function(){
                
                $('.UploadView').attr('src', '');
                $('.Prd_UploadView').hide();
                                
            });

$(document).ready(function() {
    
//document.getElementById('pro-image').addEventListener('change', readImage, false);

    $( ".preview-images-zone" ).sortable();

    $(document).on('click', '.image-cancel', function(e) {
        
        run_waitMe($('#WaitMe_photo'), 1, 'orbit');
        var no = $(this).data('no');
        var key =$(this).data('key');
        $.ajax({
                url:"<?php echo site_url('admin/properties/delete_multipleimage') ?>", //base_url() return http://localhost/tutorial/codeigniter/
                method:"POST",
                data:{ key: key},
                success:function(data)
                    {
                        $('#WaitMe_photo').waitMe('hide');
                        new jBox('Notice', {
                            //animation: 'flip',
                            animation: {
                              open: 'tada',
                              close: 'zoomIn'
                            },
                            position: {
                              x: 10,
                              y: 100
                            },
                            attributes: {
                              x: 'right',
                              y: 'bottom'
                            },
                            color: 'red',
                            autoClose: Math.random() * 8000 + 2000,
                            //title: 'Tadaaa! I\'m single',
                            content: 'Success! Image Deleted ',
                            delayOnHover: true,
                            showCountdown: true,
                            closeButton: true
                        });
                        $(".preview-image.preview-show-"+no).remove();
                       
                    }
            });
        
    });
});

function saveimage(){
    var url = $(this).attr('action');
    var data_save = $(this).serialize();

    $.ajax({
       url:"<?php site_url('upload_multiple/upload') ?>",
       type:'POST',
       dataType: 'json',
       data:new FormData(this),
       processData:false,
       contentType:false,
       async:false,
       success: function(data){}
    });          
}

var num = 1;
$('#pro-image').on('change', function(e) {

    run_waitMe($('#WaitMe_photo'), 1, 'orbit');

    var output = $(".preview-images-zone");
    
    if (window.File && window.FileList && window.FileReader) 
    {
        var files = $('#pro-image')[0].files;//or event.target.files; //FileList object
        var form_data = new FormData();

        for (var count = 0; count < files.length; count++) 
            {
                var file = files[count];
                if (!file.type.match('image')) continue;
                
                //var name = files[count].name;
                //var extension = name.split('.').pop().toLowerCase();
                //if(jQuery.inArray(extension, ['gif','png','jpg','jpeg']) === -1) continue;

                form_data.append("files[]", files[count]);
            }
        
        $.ajax({
            url:"<?php echo site_url('admin/properties/save_multipleimage/'.$impactinfo['id'] ) ?>", //base_url() return http://localhost/tutorial/codeigniter/
            method:"POST",
            data:form_data,
            contentType:false,
            cache:false,
            processData:false,
            /*beforeSend:function()
                {
                    $('.preview-images-zone').html("<label class='text-success'>Uploading...</label>");
                },*/
            success:function(data)
                {
                    new jBox('Notice', {
                        //animation: 'flip',
                        animation: {
                          open: 'tada',
                          close: 'zoomIn'
                        },
                        position: {
                          x: 10,
                          y: 100
                        },
                        attributes: {
                          x: 'right',
                          y: 'bottom'
                        },
                        color: 'green',
                        autoClose: Math.random() * 8000 + 2000,
                        //title: 'Tadaaa! I\'m single',
                        content: 'Success! Image Uploded ',
                        delayOnHover: true,
                        showCountdown: true,
                        closeButton: true
                    });
                    output.append(data);
                    $("#pro-image").val('');
                }
        });
        $('#WaitMe_photo').waitMe('hide');
    } 
    else 
    {
        console.log('Browser not support');
    }
    
});

            /*
            $(document).ready(function() {
                document.getElementById('pro-image').addEventListener('change', readImage, false);

                $( ".preview-images-zone" ).sortable();

                $(document).on('click', '.image-cancel', function() {
                    let no = $(this).data('no');
                    $(".preview-image.preview-show-"+no).remove();
                });
            });



            var num = 1;
            function readImage() {
                if (window.File && window.FileList && window.FileReader) {
                    var files = event.target.files; //FileList object
                    var output = $(".preview-images-zone");

                    for (let i = 0; i < files.length; i++) {
                        var file = files[i];
                        if (!file.type.match('image')) continue;

                        var picReader = new FileReader();

                        picReader.addEventListener('load', function (event) {
                            var picFile = event.target;
                            var html =  '<div class="preview-image preview-show-' + num + '">' +
                                        '<div class="image-cancel" data-no="' + num + '">x</div>' +
                                        '<div class="image-zone"><img id="pro-img-' + num + '" src="' + picFile.result + '"></div>' +
                                        //'<div class="tools-edit-image"><a href="javascript:void(0)" data-no="' + num + '" class="btn btn-light btn-edit-image">edit</a></div>' +
                                        '</div>';

                            output.append(html);
                            num = num + 1;
                        });

                        picReader.readAsDataURL(file);
                    }
                    $("#pro-image").val('');
                } else {
                    console.log('Browser not support');
                }
            }
            */

        </script>              
       
        <script>
            $(document).on("click",".sbmtbtn", function(e){
               
                    $('.preloader').css("display", "block");
            });

        </script>