<div class="container-fluid">
  <div class="row">
          
          <div class="col-lg-12 col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Payment </h3>
                <div class="card-tools">

                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body p-0 table-responsive ">
                <table class="table table-striped table-hover table-bordered">
                  <thead>
                  <tr>
                    <th style="width: 10px">#</th>
                    <th>Property Info</th>
                    <th>Details</th>
                    <th>Comment</th>
                    <th>Landlord Info</th>
                    <th style="width: 100px">Status</th>
                    <?php if(($_SESSION['role'] == ROLE_SUPERADMIN) ): ?> 
                    <th style="width: 250px"> Action</th>
                    <?php endif; ?>
                  </tr>
                  </thead>
                  <tbody>
                  
                      <?php if(!empty($pay)): ?>  
                      <?php $count=1; ?>
                          <?php foreach ($pay as $impacts) :?>

                              <?php 
                             
                                $status = "";
                                $label_color = array("", "danger", "warning", "primary", "success", "info");

                                if($impacts['status']==1)
                                {
                                    $status='<span class="badge bg-success">Approved</span>';
                                }
                                elseif($impacts['status']==0)
                                {
                                    $status='<span class="badge bg-warning">Pending</span>';
                                }
                                      
                              ?>
                  <tr>
                    <td><?=$count;?>.</td>
                    <td>
                      <b><a target="_blank" href="<?=site_url('properties/details/'.$impacts['slug'])?>"><?=$impacts['propertyname'];?></a></b><br>
                      Property Type : <b><?=$impacts['type'];?></b><br>
                      Bedrooms :<b><?=$impacts['bedroom'];?></b><br>
                      Bathrooms : <b><?=$impacts['bathroom'];?></b><br>
                      Toilet : <b><?=$impacts['toilet'];?></b><br>
                      Address: <small><b><?=$impacts['address']?>,<?=$impacts['cityname']?>, <?=$impacts['statename']?></b></small>
                    </td>

                    <td>
                      Name : <b><?=$impacts['name'];?></b><br>
                      Email : <b><?=$impacts['email'];?></b>
                    </td>
                    <td>
                      <b><?=$impacts['comment'];?></b>
                    </td>
                    <td>
                      <b><?=$impacts['landlordinfo']['firstname'] .' '.$impacts['landlordinfo']['firstname'];?></b><br>
                      Phone : <b><?=$impacts['landlordinfo']['phone'];?></b><br>
                      Email : <b><?=$impacts['landlordinfo']['email'];?></b>
                    </td>
                    
                    <td><?=$status;?></td>
                    <td>
                      <?php if($impacts['status'] == 0): ?> 
                      <div class="margin btn-groupp">
                        <a href="javascript:void(0);" class="btn btn-sm btn-success approve" data-get="<?=$impacts['id']?>"><i class="fa fa-align-left fa-edit"></i>Approve</a>
                        <a href="javascript:void(0);" class="btn btn-sm btn-danger impact_del" data-get="<?=$impacts['id']?>"><i class="fa fa-align-right fa-trash"></i> Declined</a>
                      </div>
                      <?php endif; ?>
                      <?php if($impacts['status'] == 1): ?> 
                      <div class="margin btn-groupp">
                        <a href="javascript:void(0);" class="btn btn-sm btn-danger impact_del" data-get="<?=$impacts['id']?>"><i class="fa fa-align-right fa-trash"></i> Declined</a>
                      </div>
                      <?php endif; ?>
                    </td>
                  </tr>
                  <?php $count++; ?>
                  <?php endforeach; ?>
                  <?php else: ?>
      
                      <tr>
                          <td class="text-center" colspan="7"><b>No Comment Created</b></td>
                      </tr>
                  <?php endif; ?>
                </tbody>

                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- ./col -->
  </div>
  <!-- /.row -->
</div>

<!-- Modal confirm order  -->
    <div class="modal" id="empty_confirmModal" style="display: none; ">
            <div class="modal-dialog">
                    <div class="modal-content">
                        
                            <div class="modal-body" >
                                <div class="col-sm-12 alert alert-danger" id="empty_confirmMessage"> </div>
                            </div>
                        
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger" id="empty_confirmOk">Ok</button>
                                <button type="button" class="btn btn-success" id="empty_confirmCancel">Cancel</button>
                            </div>
                        
                    </div>
            </div>
    </div>

<script >
    
     //  the status process button
    $(document).on("click",".approve", function(e){
        e.preventDefault();
        var empty_msg = "Are you sure you want to Approve this Comment ?";
        var row_id = $(this).data('get'); // gets value
        
        confirmDialog(empty_msg, function(){
            
            
            $.ajax({
                type:'POST',
                url:'<?= site_url('admin/comments/approve')?>',
                dataType: 'json',
                data:{
                        _id:row_id
                    },
                beforeSend: function(){
                       $('.preloader').css("display", "block");
                   },
                success:function(html){
                
                    if(html.status == '1')
                    {
                        
                    }
                    else{  }
                },
                complete:function(data){
                        // Hide image container
                        window.location.reload(); 
                        
                       }

            });
        
        }); 
        
    }); 
    
     //  the status process button
    $(document).on("click",".impact_del", function(e){
        e.preventDefault();
        var empty_msg = "Are you sure you want to Declined this Comment ?";
        var row_id = $(this).data('get'); // gets value
        
        confirmDialog(empty_msg, function(){
            
            
            $.ajax({
                type:'POST',
                url:'<?= site_url('admin/comments/delete')?>',
                dataType: 'json',
                data:{
                        _id:row_id
                    },
                beforeSend: function(){
                       $('.preloader').css("display", "block");
                   },
                success:function(html){
                
                    if(html.status == '1')
                    {
                        
                    }
                    else{  }
                },
                complete:function(data){
                        // Hide image container
                        window.location.reload(); 
                        
                       }

            });
        
        }); 
        
    }); 
    function confirmDialog(message, onConfirm){
          var fClose = function(){
          modal.modal("hide");
          };
          var modal = $("#empty_confirmModal");
          modal.modal("show");
          $("#empty_confirmMessage").empty().append(message);
          $("#empty_confirmOk").unbind().one('click', onConfirm).one('click', fClose);
          $("#empty_confirmCancel").unbind().one("click", fClose);
        }
        
    
    
</script>