<div class="container-fluid">
  <div class="row">
          
          <div class="col-lg-12 col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Property Images</h3>
                <div class="card-tools">
                  <a href="<?=site_url('admin/properties/gallaryaddform/'.$impacts['slug'])?>" class="btn btn-block btn-info btn-sm">Add Property Gallery</a>

                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body p-0 table-responsive ">
                <table class="table table-striped">
                  <thead>
                  <tr>
                    <th style="width: 10px">#</th>
                    <th>Task</th>
                    <th >Orders</th>
                    <th style="width: 100pxx">Gallary</th>
                    <th style="width: 40pxx"> Action</th>
                  </tr>
                  </thead>
                  <tbody>
                  
                      <?php if(!empty($impacts_img)): ?>  
                      <?php $count=1; ?>
                      <?php foreach ($impacts_img as $img) :?>
                  <tr>
                    <td><?=$count;?></td>
                    <td><?=$impacts['name'];?></td>
                    <td>
                      <div class="orderdiv_<?=$img['id'];?> "> &nbsp;
                        <a href="javascript:void(0);" data-toggle="tooltip"  title="Edit Order" class="jboxtooltip editorder" data-_id="<?=$img['id'];?>" data-_value="<?=$img['order_set'];?>">
                        <i class="fa fa-plus-circle"></i>
                        </a> &nbsp;<span class="orderspan_<?=$img['id'];?>"><?=$img['order_set'];?></span>
                      </div>
                        
                      </td>
                    <td>
                        <a href="<?=site_url('assets/uploads/'.$img['name'])?> " data-fancybox="images-preview" data-toolbar="false" data-small-btn="true">
                            <img class="img-thumbnail" width="50px" src="<?=site_url('assets/uploads/'.$img['name'])?> ">
                        </a>
                    </td>
                    <td>
                      <div class="margin btn-groupp">
                        <a href="javascript:void(0);" class="btn btn-danger btn-sm img_del" data-get="<?=$img['id']?>" data-key="<?=$img['name']?>" ><i class="fa fa-align-right fa-trash"></i> Delete</a>
                      </div>
                    </td>
                    
                  </tr>
                  <?php $count++; ?>
                  <?php endforeach; ?>
                  <?php else: ?>
      
                      <tr>
                          <td class="text-center" colspan="5"><b>No Impact Project Gallary Created</b></td>
                      </tr>
                  <?php endif; ?>
                </tbody>

                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- ./col -->
  </div>
  <!-- /.row -->
</div>

<!-- Modal confirm order  -->
    <div class="modal" id="empty_confirmModal" style="display: none; ">
            <div class="modal-dialog">
                    <div class="modal-content">
                        
                            <div class="modal-body" >
                                <div class="col-sm-12 alert alert-danger" id="empty_confirmMessage"> </div>
                            </div>
                        
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger" id="empty_confirmOk">Ok</button>
                                <button type="button" class="btn btn-success" id="empty_confirmCancel">Cancel</button>
                            </div>
                        
                    </div>
            </div>
    </div>

<script >
    
     //  the status process button
    $(document).on("click",".img_del", function(e){
        e.preventDefault();
        var empty_msg = "Are you sure you want to Delete this Project image?";
        var row_id = $(this).data('get'); // gets value
        var row_key = $(this).data('key'); // gets value
        
        confirmDialog(empty_msg, function(){
            
            
            $.ajax({
                type:'POST',
                url:'<?= site_url('admin/properties/deleteimg')?>',
                dataType: 'json',
                data:{
                        _id:row_id,
                        key:row_key,
                    },
                beforeSend: function(){
                       $('.preloader').css("display", "block");
                   },
                success:function(html){
                
                    if(html.status == '1')
                    {
                        
                    }
                    else{  }
                },
                complete:function(data){
                        // Hide image container
                        window.location.reload(); 
                        
                       }

            });
        
        }); 
        
    }); 
    
    function confirmDialog(message, onConfirm){
          var fClose = function(){
          modal.modal("hide");
          };
          var modal = $("#empty_confirmModal");
          modal.modal("show");
          $("#empty_confirmMessage").empty().append(message);
          $("#empty_confirmOk").unbind().one('click', onConfirm).one('click', fClose);
          $("#empty_confirmCancel").unbind().one("click", fClose);
        }
        
    // Edit Variant Impact Image Order 
    $(document).on("click",".editorder", function(e){
        e.preventDefault();
        
        var id = $(this).data('_id');
        var order = $(this).data('_value');
        
        $('[name="new_value"]').val(order);
        $('[name="new_value"]').data('_id',id); // sets value
       
        var position = $(this).offset(); //position();
        var newtop = position.top - 5;
        var newleft = position.left - 5;
        $('#order_div').css({top: newtop, left: newleft, display: 'block'});
    });
    
    // close the  Impact Image option button
    $(document).on("click","#closeorder_div", function(e){
        e.preventDefault();
        $('#order_div').hide();
    });
    
    
    // on Impact Image Order change 
    $('#order_savee').click(function (e) {

            e.preventDefault();
            
            var new_name = $('[name="new_value"]').val();
            var id   = $('[name="new_value"]').data('_id'); // gets value
            
            $(".orderdiv_"+id).find("div[data-col_id='" + id +"']").text(new_name);
            $(".orderdiv_"+id).find(" #"+cat_id +"").val(new_name);
            $(".orderdiv_"+id).find(".editcolor[data-col_id='" + cat_id +"']").data('col_name',new_name); 
            $('#order_div').hide();

    });

     // on Delivery price change 
    $(document).on("click","#order_save", function(e){
        e.preventDefault();
        
        var new_order = $('[name="new_value"]').val();
        var chrg_id   = $('[name="new_value"]').data('_id'); // gets value
        
        
            
            $.ajax({
                type: "POST",
                url: site_url+'admin/properties/imageorder',
                dataType: 'json',
                data: {
                        id: chrg_id,
                        order: new_order
                        }
            }).done(function (data) {
                if(data.status==1)
                {
                    new jBox('Notice', {
                        //animation: 'flip',
                        animation: {
                          open: 'tada',
                          close: 'zoomIn'
                        },
                        position: {
                          x: 10,
                          y: 100
                        },
                        attributes: {
                          x: 'right',
                          y: 'bottom'
                        },
                        color: 'green',
                        autoClose: Math.random() * 8000 + 2000,
                        //title: 'Tadaaa! I\'m single',
                        content: 'Success! Gallery Order Uploded ',
                        delayOnHover: true,
                        showCountdown: true,
                        closeButton: true
                    });
                    $(".orderdiv_"+chrg_id).find(" .orderspan_"+chrg_id +"").text(new_order);
                    $(".orderdiv_"+chrg_id).find(".editorder[data-_id='" + chrg_id +"']").data('_value',new_order); 
                }
                else
                {
                  new jBox('Notice', {
                        //animation: 'flip',
                        animation: {
                          open: 'tada',
                          close: 'zoomIn'
                        },
                        position: {
                          x: 10,
                          y: 100
                        },
                        attributes: {
                          x: 'right',
                          y: 'bottom'
                        },
                        color: 'red',
                        autoClose: Math.random() * 8000 + 2000,
                        //title: 'Tadaaa! I\'m single',
                        content: 'Error! Error Uploding Gallery Order ',
                        delayOnHover: true,
                        showCountdown: true,
                        closeButton: true
                    });
                }
                
            });
            $('#order_div').hide();
         
    });
    
</script>